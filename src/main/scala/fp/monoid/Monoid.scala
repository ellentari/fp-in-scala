package fp.monoid

import fp.monoid.Foldable.IdnexedSeqFoldable
import fp.tree.{Branch, Leaf, Tree}
import sun.awt.geom.AreaOp.SubOp

trait Monoid[A] {
  def op(a1: A, a2: A): A
  def zero: A
}

object Monoid {

  val stringMonoid: Monoid[String] = new Monoid[String] {
    override def op(a1: String, a2: String): String = a1 + a2
    override def zero: String = ""
  }

  def listMonoid[A]: Monoid[List[A]] = new Monoid[List[A]] {
    override def op(a1: List[A], a2: List[A]): List[A] = a1 ++ a2
    override def zero: List[A] = List()
  }

  def optionMonoid[A]: Monoid[Option[A]] = new Monoid[Option[A]] {
    override def op(a1: Option[A], a2: Option[A]): Option[A] = a1 orElse a2
    override def zero: Option[A] = None
  }

  def endoMonoid[A]: Monoid[A => A] = new Monoid[A => A] {
    override def op(f: A => A, g: A => A): A => A = f compose g
    override def zero: A => A = a => a
  }

  val intAddition: Monoid[Int] = new Monoid[Int] {
    override def op(a1: Int, a2: Int): Int = a1 + a2
    override def zero: Int = 0
  }

  val intMultiplication: Monoid[Int] = new Monoid[Int] {
    override def op(a1: Int, a2: Int): Int = a1 * a2
    override def zero: Int = 1
  }

  val booleanOr: Monoid[Boolean] = new Monoid[Boolean] {
    override def op(a1: Boolean, a2: Boolean): Boolean = a1 || a2
    override def zero: Boolean = false
  }

  val booleanAnd: Monoid[Boolean] = new Monoid[Boolean] {
    override def op(a1: Boolean, a2: Boolean): Boolean = a1 && a2
    override def zero: Boolean = true
  }

  def dual[A](m: Monoid[A]): Monoid[A] = new Monoid[A] {
    override def op(a1: A, a2: A): A = m.op(a2, a1)
    override def zero: A = m.zero
  }

  def productMonoid[A, B](am: Monoid[A], bm: Monoid[B]): Monoid[(A, B)] = new Monoid[(A, B)] {
    override def op(a1: (A, B), a2: (A, B)): (A, B) = (am.op(a1._1, a2._1), bm.op(a1._2, a2._2))
    override def zero: (A, B) = (am.zero, bm.zero)
  }

  def mapMergeMonoid[K, V](vm: Monoid[V]): Monoid[Map[K, V]] = new Monoid[Map[K, V]] {
    override def op(a1: Map[K, V], a2: Map[K, V]): Map[K, V] =
      (a1.keySet ++ a2.keySet).foldLeft(Map[K, V]())((acc, key) =>
        acc.updated(key, vm.op(a1.getOrElse(key, vm.zero), a2.getOrElse(key, vm.zero))))

    override def zero: Map[K, V] = Map()
  }

  def functionMonoid[A, B](mb: Monoid[B]): Monoid[A => B] = new Monoid[A => B] {
    override def op(a1: A => B, a2: A => B): A => B =
      a => mb.op(a1(a), a2(a))

    override def zero: A => B = _ => mb.zero
  }

  def foldMap[A, B](as: List[A], m: Monoid[B])(f: A => B): B =
    as.foldLeft(m.zero)((b, a) => m.op(f(a), b))

  def foldMap[A, B](as: IndexedSeq[A], m: Monoid[B])(f: A => B): B =
    if (as.isEmpty) m.zero
    else if (as.length == 1) f(as(0))
    else m.op(foldMap(as.take(as.length / 2), m)(f), foldMap(as.drop(as.length / 2), m)(f))

  def foldRight[A, B](as: List[A])(z: B)(f: (A, B) => B): B =
    foldMap(as, endoMonoid[B])(a => b => f(a, b))(z)

  def foldLeft[A, B](as: List[A])(z: B)(f: (B, A) => B): B =
    foldMap(as, dual(endoMonoid[B]))(a => b => f(b, a))(z)

  def isOrderedMonoid[A](ord: Ordering[A]): Monoid[Option[(A, A, Boolean)]] = new Monoid[Option[(A, A, Boolean)]] {
    override def op(a1: Option[(A, A, Boolean)], a2: Option[(A, A, Boolean)]): Option[(A, A, Boolean)] =
      (a1, a2) match {
        case (Some((a1Min, a1Max, a1Ordered)), Some((a2Min, a2Max, a2Ordered))) =>
          Some((ord.min(a1Min, a2Min), ord.max(a1Max, a2Max), a1Ordered && a2Ordered && ord.compare(a1Max, a2Min) < 0))
        case (x, None) => x
        case (None, x) => x
      }
    override def zero: Option[(A, A, Boolean)] = None
  }

  def isOrdered[A](as: List[A])(ord: Ordering[A]): Boolean =
    foldMap(as, isOrderedMonoid(ord))(a => Some((a, a, true))) forall (_._3)

  sealed trait WC
  case class Stub(chars: String) extends WC
  case class Part(lStub: String, words: Int, rStub: String) extends WC

  val wcMonoid: Monoid[WC] = new Monoid[WC] {
    override def op(a1: WC, a2: WC): WC = (a1, a2) match {
      case (Stub(c1), Stub(c2)) => Stub(c1 + c2)
      case (Stub(c1), Part(left, words, right)) => Part(c1 + left, words, right)
      case (Part(left, words, right), Stub(c2)) => Part(left, words, right + c2)
      case (Part(left1, words1, right1), Part(left2, words2, right2)) =>
        Part(left1, words1 + (if ((right1 + left2).length == 0) 0 else 1) + words2, right2)
    }

    override def zero: WC = Stub("")
  }

  def countWords(s: String): Int = {
    def wc(c: Char): WC =
      if (c.isWhitespace) Part("", 0, "")
      else Stub(c.toString)

    def unstub(s: String): Int = s.length min 1

    foldMap(s.toIndexedSeq, wcMonoid)(wc) match {
      case Stub(s) => unstub(s)
      case Part(left, words, right) => unstub(left) + words + unstub(right)
    }
  }

  def bag[A](as: IndexedSeq[A]): Map[A, Int] =
    IdnexedSeqFoldable.foldMap(as)(a => Map(a -> 1))(mapMergeMonoid(intAddition))

}
