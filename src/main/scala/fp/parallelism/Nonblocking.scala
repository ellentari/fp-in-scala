package fp.parallelism

import java.util.concurrent.atomic.AtomicReference
import java.util.concurrent.{Callable, CountDownLatch, ExecutorService}

object Nonblocking {

  sealed trait Future[A] {
    private[parallelism] def apply(cb: A => Unit): Unit
  }

  type Par[A] = ExecutorService => Future[A]

  def run[A](es: ExecutorService)(p: Par[A]): A = {
    val ref = new AtomicReference[A]
    val latch = new CountDownLatch(1)

    p(es) {a => ref.set(a); latch.countDown()}

    latch.await()

    ref.get
  }

  def unit[A](a: A): Par[A] =
    _ => new Future[A] {
      override private[parallelism] def apply(cb: A => Unit): Unit =
        cb(a)
    }

  def flatMap[A,B](p: Par[A])(f: A => Par[B]): Par[B] =
    es => new Future[B] {
      override private[parallelism] def apply(cb: B => Unit): Unit = {
        p(es) {a => f(a)(es) {b => cb(b)} }
      }
    }

  def join[A](p: Par[Par[A]]): Par[A] =
    flatMap(p)(identity)

  def map[A, B](p: Par[A])(f: A => B): Par[B] =
    flatMap(p)(a => unit(f(a)))

  def fork[A](a: => Par[A]): Par[A] =
    es => new Future[A] {
      override private[parallelism] def apply(cb: A => Unit): Unit =
        eval(es)(a(es)(cb))
    }

  def eval(es: ExecutorService)(r: => Unit): Unit =
    es.submit(new Callable[Unit] {def call = r})

  def map2[A,B,C](p: Par[A], p2: Par[B])(f: (A, B) => C): Par[C] =
    es => new Future[C] {
      override private[parallelism] def apply(cb: C => Unit): Unit = {
        var ar: Option[A] = None
        var br: Option[B] = None

        val combiner = Actor[Either[A, B]](es) {
          case Left(a) => br match {
            case None => ar = Some(a)
            case Some(b) => eval(es)(cb(f(a, b)))
          }
          case Right(b) => ar match {
            case None => br = Some(b)
            case Some(a) => eval(es)(cb(f(a, b)))
          }
        }

        p(es)(a => combiner ! Left(a))
        p2(es)(b => combiner ! Right(b))
      }
    }

  def choice[A](cond: Par[Boolean])(t: Par[A], f: Par[A]): Par[A] =
    flatMap(cond)(b => if (b) t else f)

  def choiceN[A](n: Par[Int])(choices: IndexedSeq[Par[A]]): Par[A] =
    flatMap(n)(choices)

  def choiceMap[K,V](key: Par[K])(choices: Map[K, Par[V]]): Par[V] =
    flatMap(key)(choices)
}
