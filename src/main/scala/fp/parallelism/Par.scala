package fp.parallelism

import java.util.concurrent.{Callable, ExecutorService, Future, TimeUnit}

object Par {

  type Par[A] = ExecutorService => Future[A]

  def run[A](es: ExecutorService)(a: Par[A]): Future[A] =
    a(es)

  def unit[A](a: A): Par[A] =
    _ => UnitFuture(a)

  def flatMap[A,B](p: Par[A])(f: A => Par[B]): Par[B] =
    es => f(run(es)(p).get())(es)

  def join[A](p: Par[Par[A]]): Par[A] =
    es => run(es)(run(es)(p).get())

  def map2[A,B,C](p1: Par[A], p2: Par[B])(f: (A, B) => C): Par[C] =
    es => {
      val a = p1(es)
      val b = p2(es)

      CombinedFuture(a, b, f)
    }

  def fork[A](a: => Par[A]): Par[A] =
    es =>
      es.submit(new Callable[A] {
        override def call(): A = a(es).get
      })

  def delay[A](fa: => Par[A]): Par[A] =
    es => fa(es)

  def lazyUnit[A](a: A): Par[A] =
    fork(unit(a))

  def asyncF[A,B](f: A => B): A => Par[B] =
    a => lazyUnit(f(a))

  def map[A,B](p: Par[A])(f: A => B): Par[B] =
    map2(p, unit())((a, _) => f(a))

  def map3[A,B,C,D](p1: Par[A], p2: Par[B], p3: Par[C])(f: (A,B,C) => D): Par[D] =
    map2(map2(p1, p2)((a, b) => (a, b)), p3)((ab, c) => f(ab._1, ab._2, c))

  def map4[A,B,C,D,E](p1: Par[A], p2: Par[B], p3: Par[C], p4: Par[D])(f: (A,B,C,D) => E): Par[E] =
    map2(map3(p1, p2, p3)((a, b, c) => (a, b, c)), p4)((abc, d) => f(abc._1, abc._2, abc._3, d))

  def map5[A,B,C,D,E,F](p1: Par[A], p2: Par[B], p3: Par[C], p4: Par[D], p5: Par[E])(f: (A,B,C,D,E) => F): Par[F] =
    map2(map4(p1, p2, p3, p4)((a, b, c, d) => (a, b, c, d)), p5)((abcd, e) => f(abcd._1, abcd._2, abcd._3, abcd._4, e))

  def parMap[A,B](ps: List[A])(f: A => B): Par[List[B]] = fork {
    val fbs = ps.map(asyncF(f))
    sequence(fbs)
  }

  def parFilter[A](as: List[A])(f: A => Boolean): Par[List[A]] = {
    val filtered: Par[List[List[A]]] = parMap(as)(a => if(f(a)) List(a) else List())
    map(filtered)(_.flatten)
  }

  def sequence[A](ps: List[Par[A]]): Par[List[A]] =
    ps.foldLeft(Par.unit[List[A]](List()))((acc, a) => map2(a, acc)(_ :: _))

  def sum(ints: IndexedSeq[Int]): Par[Int] =
    if (ints.size <= 1)
      Par.unit(ints.headOption.getOrElse(0))
    else {
      val (left, right) = ints.splitAt(ints.length / 2)
      Par.map2(fork(sum(left)), fork(sum(right))) (_ + _)
    }

  def sortPar(parList: Par[List[Int]]): Par[List[Int]] =
    map(parList)(_.sorted)

  def choice[A](cond: Par[Boolean])(t: Par[A], f: Par[A]): Par[A] =
    flatMap(cond)(b => if (b) t else f)

  def choiceN[A](n: Par[Int])(choices: IndexedSeq[Par[A]]): Par[A] =
    flatMap(n)(choices)

  def choiceMap[K,V](key: Par[K])(choices: Map[K, Par[V]]): Par[V] =
    flatMap(key)(choices)

  private case class UnitFuture[A](get: A) extends Future[A] {
    override def isDone: Boolean = true
    override def get(timeout: Long, unit: TimeUnit): A = get
    override def cancel(mayInterruptIfRunning: Boolean): Boolean = false
    override def isCancelled: Boolean = false
  }

  private case class CombinedFuture[A,B,C](fa: Future[A], fb: Future[B], f: (A,B) => C) extends Future[C] {
    override def cancel(mayInterruptIfRunning: Boolean): Boolean =
      fa.cancel(mayInterruptIfRunning) && fb.cancel(mayInterruptIfRunning)

    override def isCancelled: Boolean = fa.isCancelled || fb.isCancelled

    override def isDone: Boolean = fa.isDone && fb.isDone

    override def get(): C = f(fa.get, fb.get)

    override def get(timeout: Long, unit: TimeUnit): C = {
      val startTime = System.nanoTime
      val a = fa.get(timeout, unit)
      val endTime = System.nanoTime

      val aDuration = unit.convert(endTime - startTime, TimeUnit.NANOSECONDS)

      val b = fb.get(timeout - aDuration, unit)

      f(a, b)
    }
  }

}
