package fp.applicative

import fp.monad.Functor
import fp.monoid.Monoid

trait Applicative[F[_]] extends Functor[F] {

  def unit[A](a: => A): F[A]

  def apply[A,B](fab: F[A => B])(fa: F[A]): F[B] =
    map2(fa, fab)((a, f) => f(a))

  def map2[A,B,C](fa: F[A], fb: F[B])(f: (A, B) => C): F[C] = {
    val b2c: F[B => C] = map(fa)(f.curried)
    apply(b2c)(fb)
  }

  def map3[A,B,C,D](fa: F[A], fb: F[B], fc: F[C])(f: (A, B, C) => D): F[D] = {
    val b2c2d: F[B => C => D] = map(fa)(f.curried)
    val c2d: F[C => D] = apply(b2c2d)(fb)
    apply(c2d)(fc)
  }

  def map4[A,B,C,D,E](fa: F[A], fb: F[B], fc: F[C], fd: F[D])(f: (A, B, C, D) => E): F[E] = {
    val b2c2d2e: F[B => C => D => E] = map(fa)(f.curried)
    val c2d2e: F[C => D => E] = apply(b2c2d2e)(fb)
    val d2e = apply(c2d2e)(fc)
    apply(d2e)(fd)
  }

  override def map[A,B](fa: F[A])(f: A => B): F[B] =
    apply(unit(f))(fa)

  def traverse[A,B](as: List[A])(f: A => F[B]): F[List[B]] =
    as.foldRight(unit(List[B]()))((a, fbs) => map2(f(a), fbs)(_ :: _))

  def sequence[A](as: List[F[A]]): F[List[A]] =
    traverse(as)(identity)

  def sequenceMap[K,V](ofa: Map[K, F[V]]): F[Map[K, V]] =
    ofa.foldRight(unit(Map[K,V]()))((e, map) => map2(e._2, map)((v, m) => m + (e._1 -> v)))

  def replicateM[A](n: Int, fa: F[A]): F[List[A]] =
    sequence(List.fill(n)(fa))

  def filterM[A](la: List[A])(f: A => F[Boolean]): F[List[A]] = {
    map(sequence(la map (a => map(f(a))(bool => if (bool) List(a) else List()))))(_.flatten)
  }

  def product[A,B](fa: F[A], fb: F[B]): F[(A, B)] =
    map2(fa, fb)((_,_))


}

object Applicative {

  case class Id[A](value: A)

  type Const[M,B] = M

  val idApplicative: Applicative[Id] = new Applicative[Id] {
    override def unit[A](a: => A): Id[A] = Id(a)
    override def apply[A, B](fab: Id[A => B])(fa: Id[A]): Id[B] =
      Id(fab.value(fa.value))
  }

  def monoidApplicative[M](M: Monoid[M]): Applicative[({type f[x] = Const[M, x]})#f] =
    new Applicative[({type f[x] = Const[M, x]})#f] {
      override def unit[A](a: => A): M = M.zero
      override def map2[A, B, C](fa: M, fb: M)(f: (A, B) => C): M = M.op(fa, fb)
    }

  def validationApplicative[E]: Applicative[({type f[x] = Validation[E, x]})#f] =
    new Applicative[({type f[x] = Validation[E, x]})#f] {
      override def unit[A](a: => A): Validation[E, A] = Success(a)
      override def map2[A, B, C](fa: Validation[E, A], fb: Validation[E, B])(f: (A, B) => C): Validation[E, C] =
        Validation.map2(fa, fb)(f)
    }

  def product[H[_], G[_]](H: Applicative[H], G: Applicative[G]): Applicative[({type f[x] = (H[x], G[x])})#f] =
    new Applicative[({type f[x] = (H[x], G[x])})#f] {
      override def unit[A](a: => A): (H[A], G[A]) =
        (H.unit(a), G.unit(a))

      override def apply[A, B](fab: (H[A => B], G[A => B]))(fa: (H[A], G[A])): (H[B], G[B]) =
        (H.apply(fab._1)(fa._1), G.apply(fab._2)(fa._2))
    }

  def compose[H[_], G[_]](H: Applicative[H], G: Applicative[G]): Applicative[({type f[x] = H[G[x]]})#f] =
    new Applicative[({type f[x] = H[G[x]]})#f] {
      override def unit[A](a: => A): H[G[A]] =
        H.unit(G.unit(a))

      override def apply[A, B](fab: H[G[A => B]])(fa: H[G[A]]): H[G[B]] =
        H.map2(fab, fa)((gab, ga) => G.apply(gab)(ga))
    }
}
