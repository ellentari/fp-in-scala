package fp.applicative

import fp.applicative.Applicative.{Const, monoidApplicative}
import fp.monad.{Functor, Monad}
import fp.monoid.{Foldable, Monoid}
import fp.state.RNG.State

trait Traverse[F[_]] extends Functor[F] with Foldable[F] { self =>

  def traverse[G[_], A, B](fa: F[A])(f: A => G[B])(implicit A: Applicative[G]): G[F[B]] =
    sequence(map(fa)(f))

  def traverseS[S,A,B](fa: F[A])(f: A => State[S, B]): State[S, F[B]] =
    traverse[({type f[x] = State[S,x]})#f, A, B](fa)(f)(Monad.stateMonad)

  def sequence[G[_], A](fa: F[G[A]])(implicit A: Applicative[G]): G[F[A]] =
    traverse(fa)(identity)

  override def map[A,B](fa: F[A])(f: A => B): F[B] =
    traverse(fa)(a => Applicative.Id(f(a)))(Applicative.idApplicative).value

  override def foldMap[A, B](as: F[A])(f: A => B)(m: Monoid[B]): B =
    traverse[({type f[x] = Const[B,x]})#f, A, Nothing](as)(f)(monoidApplicative(m))

  override def toList[A](fa: F[A]): List[A] =
    mapAccum(fa, List[A]())((a, s) => ((), a :: s))._2.reverse

  override def foldLeft[A, B](fa: F[A])(z: B)(f: (B, A) => B): B =
    mapAccum(fa, z)((a, b) => ((), f(b, a)))._2

  def mapAccum[S,A,B](fa: F[A], s: S)(f: (A, S) => (B, S)): (F[B], S) =
    traverseS(fa)(a => (for {
      s1 <- State.get[S]
      (b, s2) = f(a, s1)
      _ <- State.set(s2)
    } yield b)).run(s)

  def reverse[A](fa: F[A]): F[A] =
    mapAccum(fa, toList(fa).reverse)((_, as) => (as.head, as.tail))._1

  def zipWithIndex[A](fa: F[A]): F[(A, Int)] =
    mapAccum(fa, 0)((a, s) => ((a, s), s + 1))._1

  def zip[A,B](fa: F[A], fb: F[B]): F[(A, B)] =
    (mapAccum(fa, toList(fb)) {
      case (a, Nil) => sys.error("zip: Incompatible shapes")
      case (a, b :: bs) => ((a, b), bs)
    })._1

  def zipLeft[A,B](fa: F[A], fb: F[B]): F[(A, Option[B])] =
    (mapAccum(fa, toList(fb)) {
      case (a, Nil) => ((a, None), Nil)
      case (a, b :: bs) => ((a, Some(b)), bs)
    })._1

  def zipRight[A,B](fa: F[A], fb: F[B]): F[(Option[A], B)] =
    (mapAccum(fb, toList(fa)) {
      case (b, Nil) => ((None, b), Nil)
      case (b, a :: as) => ((Some(a), b), as)
    })._1

  def fuse[G[_],H[_],A,B](fa: F[A])
                         (f: A => G[B], g: A => H[B])
                         (G: Applicative[G], H: Applicative[H]): (G[F[B]], H[F[B]]) = {
    traverse[({type f[x] = (G[x],H[x])})#f, A, B](fa)(a => (f(a), g(a)))(Applicative.product(G, H))
  }

  def compose[H[_]](implicit H: Traverse[H]): Traverse[({type f[x] = F[H[x]]})#f] =
    new Traverse[({type f[x] = F[H[x]]})#f] {
      override def traverse[G[_], A, B](fa: F[H[A]])(f: A => G[B])(implicit A: Applicative[G]): G[F[H[B]]] = {
        self.traverse(fa)(ha => H.traverse(ha)(f))
      }
    }
}

object Traverse {

  case class Tree[+A](head: A, tail: List[Tree[A]])

  val listTraverse: Traverse[List] = new Traverse[List] {
    override def traverse[G[_], A, B](fa: List[A])(f: A => G[B])(implicit A: Applicative[G]): G[List[B]] = {
      fa.foldRight(A.unit(List[B]()))((a, acc) => A.map2(f(a), acc)(_ :: _))
    }
  }

  val optionTraverse: Traverse[Option] = new Traverse[Option] {
    override def traverse[G[_], A, B](fa: Option[A])(f: A => G[B])(implicit A: Applicative[G]): G[Option[B]] = {
      fa.fold(A.unit(None: Option[B]))(a => A.map(f(a))(Some(_)))
    }
  }

  def mapTraverse[K]: Traverse[({type f[x] = Map[K,x]})#f] =
    new Traverse[({type f[x] = Map[K, x]})#f] {
      override def traverse[G[_], A, B](fa: Map[K, A])(f: A => G[B])(implicit A: Applicative[G]): G[Map[K, B]] =
        fa.foldRight(A.unit(Map[K,B]()))((e, acc) => A.map2(f(e._2), acc)((b, map) => map + (e._1 -> b)))
    }

  val treeTraverse: Traverse[Tree] = new Traverse[Tree] {
    override def traverse[G[_], A, B](fa: Tree[A])(f: A => G[B])(implicit A: Applicative[G]): G[Tree[B]] = {
      val gHead = f(fa.head)
      val gTail = listTraverse.traverse(fa.tail)(a => traverse(a)(f))

      A.map2(gHead, gTail)(Tree(_,_))
    }
  }



}
