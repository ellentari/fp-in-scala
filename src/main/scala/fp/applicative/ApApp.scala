package fp.applicative

import java.time.LocalDate
import java.time.format.DateTimeParseException

import fp.monad.Monad._
import fp.applicative.Traverse._

object ApApp extends App {

  case class WebForm(name: String, birthday: LocalDate, phone: String)

  def validName(name: String): Validation[String, String] =
    if (name != "") Success(name)
    else Failure("Name can not be empty")

  def validBirthday(birthday: String): Validation[String, LocalDate] =
    try {
      import java.time.format.DateTimeFormatter

      Success(LocalDate.parse(birthday, DateTimeFormatter.ofPattern("yyyy-MM-dd")))
    } catch  {
      case e: DateTimeParseException => Failure("Birthdate must be in the form yyyy-MM-dd")
    }

  def validPhone(phone: String): Validation[String, String] =
    if (phone.matches("[0-9]{10}")) Success(phone)
    else Failure("Phone number must be 10 digits")

  def validWebForm(name: String, birthday: String, phone: String): Validation[String, WebForm] =
    Applicative.validationApplicative[String]
      .map3(
        validName(name),
        validBirthday(birthday),
        validPhone(phone)
      )(WebForm)

  println(validWebForm("name", "1999-01-01", "1234567890"))
  println(validWebForm("", "adasdasd", "dgfsfg"))

  println(Traverse.listTraverse.zipWithIndex(List(1, 2, 3)))

  println(Traverse.listTraverse.fuse(List(1, 2, 3))(i => Option(i), i => Option(i))(optionMonad, optionMonad))


  private val composedOptTraverse = composeM(listMonad, optionMonad, optionTraverse)
  private val someInts = List(Some(1), Some(2), Some(3))
  println(composedOptTraverse.flatMap(someInts)(a => List(Some(a), Some(a * 2))))

  private val composedListTraverse = composeM(optionMonad, listMonad, listTraverse)
  private val someListInts = Some(List(1, 2, 3))
  println(composedListTraverse.flatMap(someListInts)(a => Some(List(a, a * 2))))

}
