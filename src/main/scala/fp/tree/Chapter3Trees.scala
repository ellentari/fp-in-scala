package fp.tree

import fp.tree.Tree._

object Chapter3Trees extends App {

  val tree = Branch(Branch(Leaf(1), Leaf(2)), Branch(Leaf(3), Leaf(4)))

  println("size:")
  println(size(tree))

  println()

  println("sizeViaFold:")
  println(sizeViaFold(tree))

  println()

  println("maximum:")
  println(maximum(tree))

  println()

  println("maximumViaFold:")
  println(maximumViaFold(tree))

  println()

  println("depth:")
  println(depth(tree))

  println()

  println("depthViaFold:")
  println(depthViaFold(tree))

  println()

  println("map:")
  println(map(tree)(_ * 2))

  println()

  println("mapViaFold:")
  println(mapViaFold(tree)(_ * 2))

}
