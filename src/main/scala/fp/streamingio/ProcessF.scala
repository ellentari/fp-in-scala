package fp.streamingio

import java.io.{BufferedReader, FileReader, FileWriter}
import java.util.concurrent.{ExecutorService, Executors}

import fp.effect.IO
import fp.effect.IO._
import fp.monad.Monad

import scala.annotation.tailrec

trait ProcessF[F[_], O] {

  import ProcessF._

  def onHalt(f: Throwable => ProcessF[F, O]): ProcessF[F, O] = this match {
    case Halt(e) => Try(f(e))
    case Emit(h, t) => Emit(h, t.onHalt(f))
    case Await(req, recv) => Await(req, recv andThen (_ onHalt f))
  }

  def onCompete(p: => ProcessF[F, O]): ProcessF[F, O] =
    this.onHalt {
      case End => p.asFinalizer
      case err => p.asFinalizer ++ Halt(err)
    }

  def ++(p: => ProcessF[F, O]): ProcessF[F, O] =
    this.onHalt {
      case End => p
      case err => Halt(err)
    }

  def repeat: ProcessF[F, O] = this ++ this.repeat

  def flatMap[O2](f: O => ProcessF[F, O2]): ProcessF[F, O2] = this match {
    case Halt(err) => Halt(err)
    case Emit(h, t) => Try(f(h)) ++ t.flatMap(f)
    case Await(req, recv) => Await(req, recv andThen (_ flatMap f))
  }

  def map[O2](f: O => O2): ProcessF[F, O2] = this match {
    case Halt(e) => Halt(e)
    case Emit(h, t) => Try { Emit(f(h), t map f) }
    case Await(req, recv) =>
      Await(req, recv andThen (_.map(f)))

  }

  def runLong(implicit F: MonadCatch[F]): F[IndexedSeq[O]] = {

    def go(cur: ProcessF[F, O], acc: IndexedSeq[O]): F[IndexedSeq[O]] =
      cur match {
        case Emit(h, t) => go(t, acc :+ h)
        case Halt(End) => F.unit(acc)
        case Halt(err) => F.fail(err)
        case Await(req, recv: (Either[Throwable, Any] => ProcessF[F, O])) => F.flatMap(F.attempt(req)) {
          e => go(Try(recv(e)), acc)
        }
      }

    go(this, IndexedSeq())
  }

  def asFinalizer: ProcessF[F, O] = this match {
    case Emit(h, t) => Emit(h, t.asFinalizer)
    case Halt(err) => Halt(err)
    case Await(req, recv: (Either[Throwable, Any] => ProcessF[F, O])) => await(req) {
      case Left(Kill) => this.asFinalizer
      case x => recv(x)
    }
  }

  final def drain[O2]: ProcessF[F, O2] = this match {
    case Halt(err) => Halt(err)
    case Emit(_, t) => t.drain
    case Await(req, recv) => Await(req, recv andThen (_.drain))
  }

  @tailrec
  final def kill[O2]: ProcessF[F, O2] = this match {
    case Halt(err) => Halt(err)
    case Emit(_, t) => t.kill
    case Await(_, recv) => recv(Left(Kill)).drain.onHalt {
      case Kill => Halt(End)
      case e => Halt(e)
    }
  }

  def |>[O2](p2: Process1[O, O2]): ProcessF[F, O2] = p2 match {
    case Halt(e2) => this.kill onHalt (e => Halt(e) ++ Halt(e2))
    case Emit(h, t) => Emit(h, this |> t)
    case Await(_, recv2) => this match {
      case Halt(e) => Halt(e) |> recv2(Left(e))
      case Emit(h, t) => t |> Try(recv2(Right(h)))
      case Await(req, recv) => Await(req, recv andThen (_ |> p2))
    }
  }

  def filter(p: O => Boolean): ProcessF[F, O] =
    this |> ProcessF.filter1(p)

  def take(n: Int): ProcessF[F, O] =
    this |> ProcessF.take1(n)

  def takeWhile(p: O => Boolean): ProcessF[F, O] =
    this |> ProcessF.takeWhile1(p)

  def zipWith[O2, O3](p2: ProcessF[F, O2])(f: (O, O2) => O3): ProcessF[F, O3] =
    (this tee p2)(ProcessF.zipWith(f))

  def zip[O2](p2: ProcessF[F, O2]): ProcessF[F, (O, O2)] =
    (this tee p2)(ProcessF.zip)

  def tee[O2, O3](p2: ProcessF[F, O2])(t: Tee[O, O2, O3]): ProcessF[F, O3] =
    t match {
      case Halt(e) => this.kill[O3] onCompete p2.kill onCompete Halt(e)
      case Emit(h, t) => Emit(h, (this tee p2)(t))
      case Await(side, recv) => side.get match {

        case Left(_) => this match {
          case Halt(e) => p2.kill onCompete Halt(e)
          case Emit(o, ot) => (ot tee p2)(Try(recv(Right(o))))
          case Await(reqL, recvL) =>
            Await(reqL, recvL andThen (this2 => this2.tee(p2)(t)))
        }

        case Right(_) => p2 match {
          case Halt(e) =>this.kill onCompete Halt(e)
          case Emit(o2, o2t) => (this tee o2t)(Try(recv(Right(o2))))
          case Await(reqR, recvR) =>
            Await(reqR, recvR andThen (p3 => this.tee(p3)(t)))
        }

      }
    }

  def to(sink: Sink[F, O]): ProcessF[F, Unit] =
    through(sink)

  def through[O2](p2: ProcessF[F, O => ProcessF[F, O2]]): ProcessF[F, O2] =
    join(this.zipWith(p2)((o, f) => f(o)))

}

object ProcessF {

  case class Await[F[_], A, O](req: F[A], recv: Either[Throwable, A] => ProcessF[F, O]) extends ProcessF[F, O]

  case class Emit[F[_], O](head: O, tail: ProcessF[F, O]) extends ProcessF[F, O]

  case class Halt[F[_], O](err: Throwable) extends ProcessF[F, O]

  case object Kill extends Throwable

  case object End extends Throwable

  def Try[F[_], O](p: => ProcessF[F, O]): ProcessF[F, O] =
    try p
    catch {
      case e: Throwable => Halt(e)
    }

  def await[F[_], A, O](req: F[A])(recv: Either[Throwable, A] => ProcessF[F, O]): ProcessF[F, O] =
    Await(req, recv)

  def runLog[O](src: ProcessF[IO, O]): IO[IndexedSeq[O]] = IO {
    implicit val E: ExecutorService = Executors.newFixedThreadPool(4)

    @tailrec
    def go(cur: ProcessF[IO, O], acc: IndexedSeq[O]): IndexedSeq[O] =
      cur match {
        case Emit(h, t) => go(t, acc :+ h)
        case Halt(End) => acc
        case Halt(err) => throw err
        case Await(req, recv) =>
          val next =
            try recv(Right(unsafePerformIO(req)))
            catch {
              case e: Throwable => recv(Left(e))
            }
          go(next, acc)
      }

    try go(src, IndexedSeq())
    finally E.shutdown()
  }

  val p: ProcessF[IO, String] =
    await(IO(new BufferedReader(new FileReader("lines.txt")))) {
      case Right(b) =>
        lazy val next: ProcessF[IO, String] = await(IO(b.readLine)) {
          case Left(e) => await(IO(b.close()))(_ => Halt(e))
          case Right(line) =>
            if (line eq null) Halt(End)
            else Emit(line, next)
        }
        next
      case Left(e) => Halt(e)
    }

  trait MonadCatch[F[_]] extends Monad[F] {
    def attempt[A](f: F[A]): F[Either[Throwable, A]]

    def fail[A](t: Throwable): F[A]
  }

  def resource[R, O](acquire: IO[R])
                    (use: R => ProcessF[IO, O])
                    (release: R => ProcessF[IO, O]): ProcessF[IO, O] =
    eval(acquire).flatMap(r => use(r).onCompete(release(r)))

  def eval[F[_], A](fa: F[A]): ProcessF[F, A] =
    await[F, A, A](fa) {
      case Right(a) => Emit(a, Halt(End))
      case Left(err) => Halt(err)
    }

  def eval_[F[_], A, B](fa: F[A]): ProcessF[F, B] =
    eval(fa).drain[B]

  def lines(filename: String): ProcessF[IO, String] =
    resource
    {IO(io.Source.fromFile(filename))}
    { src =>
      lazy val iter = src.getLines()

      def step: Option[String] = if (iter.hasNext) Some(iter.next()) else None

      lazy val lines: ProcessF[IO, String] = eval(IO(step)).flatMap {
        case None => Halt(End)
        case Some(line) => Emit(line, lines)
      }

      lines
    }
    { src => eval_(IO {src.close()}) }

  case class Is[I]() {

    sealed trait f[x]

    val Get: f[I] = new f[I] {}
  }

  type Process1[I, O] = ProcessF[Is[I]#f, O]

  def Get[I]: Is[I]#f[I] =
    Is[I]().Get

  def halt1[I, O]: Process1[I, O] =
    Halt[Is[I]#f, O](End)

  def emit1[I, O](h: O, tail: Process1[I, O] = halt1[I, O]): Process1[I, O] =
    Emit(h, tail)

  def await1[I, O](recv: I => Process1[I, O], fallback: Process1[I, O] = halt1[I, O]): Process1[I, O] =
    Await(Get[I], (e: Either[Throwable, I]) => e match {
      case Left(End) => fallback
      case Left(err) => Halt(err)
      case Right(i) => Try(recv(i))
    })

  def lift1[I, O](f: I => O): Process1[I, O] =
    await1[I, O](i => emit1(f(i))).repeat

  def filter1[I, O](p: I => Boolean): Process1[I, I] =
    await1[I, I](i =>
      if (p(i)) emit1(i)
      else halt1
    ).repeat

  def take1[I](n: Int): Process1[I, I] =
    if (n <= 0) halt1
    else await1[I, I](i => emit1(i, take1(n - 1)))

  def takeWhile1[I](p: I => Boolean): Process1[I, I] = {
    await1[I, I](i =>
      if (p(i)) takeWhile1(p)
      else halt1
    )
  }

  case class T[I1, I2]() {

    sealed trait f[x] {
      def get: Either[I1 => x, I2 => x]
    }

    val L: f[I1] = new f[I1] {
      override def get: Either[I1 => I1, I2 => I1] = Left(identity)
    }
    val R: f[I2] = new f[I2] {
      override def get: Either[I1 => I2, I2 => I2] = Right(identity)
    }
  }

  def L[I1, I2]: T[I1, I2]#f[I1] = T[I1, I2]().L

  def R[I1, I2]: T[I1, I2]#f[I2] = T[I1, I2]().R

  type Tee[I1, I2, O] = ProcessF[T[I1, I2]#f, O]

  def haltT[I1, I2, O]: Tee[I1, I2, O] =
    Halt[T[I1, I2]#f, O](End)

  def awaitL[I1, I2, O](recv: I1 => Tee[I1, I2, O], fallback: Tee[I1, I2, O] = haltT[I1, I2, O]): Tee[I1, I2, O] =
    await[T[I1, I2]#f, I1, O](L) {
      case Left(End) => fallback
      case Left(err) => Halt(err)
      case Right(i1) => Try(recv(i1))
    }

  def awaitR[I1, I2, O](recv: I2 => Tee[I1, I2, O], fallback: Tee[I1, I2, O] = haltT[I1, I2, O]): Tee[I1, I2, O] =
    await[T[I1, I2]#f, I2, O](R) {
      case Left(End) => fallback
      case Left(err) => Halt(err)
      case Right(i2) => Try(recv(i2))
    }

  def emitT[I1, I2, O](h: O, t: Tee[I1, I2, O] = haltT[I1, I2, O]): Tee[I1, I2, O] =
    Emit[T[I1, I2]#f, O](h, t)

  def zipWith[I1, I2, O](f: (I1, I2) => O): Tee[I1, I2, O] =
    awaitL[I1, I2, O](i1 =>
    awaitR[I1, I2, O](i2 => emitT(f(i1, i2)))).repeat

  def zip[I1, I2, O]: Tee[I1, I2, (I1, I2)] =
    zipWith((_,_))

  type Sink[F[_], O] = ProcessF[F, O => ProcessF[F, Unit]]

  def fileW(file: String, append: Boolean = false): Sink[IO, String] =
    resource[FileWriter, String => ProcessF[IO, Unit]]
      {IO {new FileWriter(file, append)} }
      { w => constant {(s: String) => eval[IO, Unit](IO(w.write(s))) } }
      { w => eval_(IO(w.close()))}

  def constant[A](a: A): ProcessF[IO, A] =
    eval[IO, A](IO(a)).repeat

  def join[F[_], O](p: ProcessF[F, ProcessF[F, O]]): ProcessF[F, O] =
    p flatMap identity

  def id[I]: Process1[I,I] =
    await1[I,I](i => emit1(i, id))

  def intersperse[I](sep: I): Process1[I, I] =
    await1[I, I](i => emit1(i) ++ id.flatMap(i => emit1(sep) ++ emit1(i)))

  type Channel[F[_], I, O] = ProcessF[F, I => ProcessF[F, O]]
}