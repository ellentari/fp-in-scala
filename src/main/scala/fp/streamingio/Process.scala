package fp.streamingio

import fp.monad.Monad

sealed trait Process[I,O] {

  import Process._

  def apply(s: Stream[I]): Stream[O] = this match {
    case Halt() => Stream()
    case Emit(head, tail) => head #:: tail(s)
    case Await(recv) => s match {
      case head #:: tail => recv(Some(head))(tail)
      case xs => recv(None)(xs)
    }
  }

  def repeat: Process[I,O] = {
    def go(p: Process[I,O]): Process[I,O] = p match {
      case Halt() => go(this)
      case Emit(head, tail) => Emit(head, go(tail))
      case Await(recv) => Await {
        case None => recv(None)
        case i => go(recv(i))
      }
    }

    go(this)
  }

  def |>[O2](p2: Process[O,O2]): Process[I,O2] = p2 match {
    case Halt() => Halt()
    case Emit(head, tail) => Emit(head, this |> tail)
    case Await(recv) => this match {
      case Halt() => Halt() |> recv(None)
      case Emit(h1, t1) => t1 |> recv(Some(h1))
      case Await(recv1) => Await((i: Option[I]) => recv1(i) |> p2)
    }
  }

  def ++(p2: => Process[I,O]): Process[I,O] = this match {
    case Halt() => p2
    case Emit(head, tail) => Emit(head, tail ++ p2)
    case Await(recv) => Await(recv andThen (_ ++ p2))
  }

  def map[O2](f: O => O2): Process[I, O2] =
    this |> lift(f)

  def flatMap[O2](f: O => Process[I,O2]): Process[I,O2] = this match {
    case Halt() => Halt()
    case Emit(head, tail) => f(head) ++ tail.flatMap(f)
    case Await(recv) => Await(recv andThen (_ flatMap f))
  }

  def zip[O2](p: Process[I,O2]): Process[I,(O, O2)] =
    zipWith(p)((_,_))

  def zipWith[O2,O3](p: Process[I,O2])(f: (O, O2) => O3): Process[I,O3] =
    Process.zipWith(this, p)(f)

  def zipWithIndex: Process[I,(O, Int)] =
    Process.zipWithIndex(this)

}

case class Emit[I,O](head: O, tail: Process[I,O] = Halt[I,O]()) extends Process[I,O]
case class Await[I,O](recv: Option[I] => Process[I,O]) extends Process[I,O]
case class Halt[I,O]() extends Process[I,O]

object Process {

  def monad[I]: Monad[({ type f[x] = Process[I,x]})#f] =
    new Monad[({type f[x] = Process[I, x]})#f] {
      override def unit[A](a: => A): Process[I, A] = Emit(a)
      override def flatMap[A, B](fa: Process[I, A])(f: A => Process[I, B]): Process[I, B] = fa flatMap f
    }

  def await[I,O](f: I => Process[I,O], fallback: => Process[I,O] = Halt[I,O]()): Process[I,O] =
    Await {
      case Some(i) => f(i)
      case _ => fallback
    }

  def liftOne[I,O](f: I => O): Process[I,O] =
    await(i => Emit(f(i)))

  def lift[I,O](f: I => O): Process[I,O] =
    liftOne(f).repeat

  def identity[I]: Process[I,I] =
    lift(i => i)

  def filterOne[I](p: I => Boolean): Process[I,I] =
    Await {
      case Some(i) if p(i) => Emit(i)
      case _ => Halt()
    }

  def filter[I](p: I => Boolean): Process[I,I] =
    filterOne(p).repeat

  def sum: Process[Double,Double] = {
    def go(acc: Double): Process[Double,Double] =
      await(i => Emit(acc + i, go(acc + i)))

    go(0.0)
  }

  def count[I]: Process[I, Int] = {
    def go(n: Int): Process[I, Int] =
      await(_ => Emit(n + 1, go(n + 1)))

    go(0)
  }

  def mean: Process[Double,Double] = {
    def go(n: Int, sum: Double): Process[Double,Double] =
      await(i => Emit((sum + i) / (n + 1), go(n + 1, sum + i)))

    go(0, 0.0)
  }

  def mean2: Process[Double, Double] =
    (sum zipWith count)(_ / _)

  def loop[S,I,O](s: S)(f: (I, S) => (O, S)): Process[I, O] =
    await(i => f(i, s) match {
      case (o, s1) => Emit(o, loop(s1)(f))
    })

  def sumViaLoop: Process[Double,Double] =
    loop(0.0) {
      case (i, acc) => (acc + i, acc + i)
    }

  def countViaLoop[I]: Process[I,Int] =
    loop(0) {
      case (_, n) => (n + 1, n + 1)
    }

  def take[I](n: Int): Process[I,I] = {
    def go(k: Int): Process[I,I] =
      Await {
        case Some(i) if k < n => Emit(i, go(k + 1))
        case _ => Halt()
      }

    go(0)
  }

  def drop[I](n: Int): Process[I, I] = {
    def go(k: Int): Process[I,I] =
      Await {
        case Some(_) if k < n => go(k + 1)
        case Some(i) => Emit(i, go(k + 1))
        case _ => Halt()
      }

    go(0)
  }

  def takeWhile[I](p: I => Boolean): Process[I,I] = {
    def go: Process[I,I] =
      Await {
        case Some(i) if p(i) => Emit(i, go)
        case _ => Halt()
      }

    go
  }

  def dropWhile[I](p: I => Boolean): Process[I,I] = {
    def go(matched: Boolean): Process[I,I] =
      Await {
        case Some(i) if matched || !p(i)  => Emit(i, go(matched = true))
        case Some(i) if p(i) => go(matched = false)
        case _ => Halt()
      }

    go(matched = false)
  }

  def zipWithIndex[I,O](p: Process[I,O]): Process[I,(O, Int)] =
    p zip (count map (_ - 1))

  def zipWith[I,O1,O2,O3](p1: Process[I,O1], p2: Process[I,O2])(f: (O1, O2) => O3): Process[I,O3] = (p1, p2) match {
    case (Halt(), _) => Halt()
    case (_, Halt()) => Halt()
    case (Emit(h1, t1), Emit(h2, t2)) => Emit(f(h1, h2), t1.zipWith(t2)(f))
    case (Await(recv), _) =>
      Await((oi: Option[I]) => zipWith(recv(oi), feed(oi)(p2))(f))
    case (_, Await(recv)) =>
      Await((oi: Option[I]) => zipWith(feed(oi)(p1), recv(oi))(f))
  }

  def feed[I,O](oi: Option[I])(p: Process[I,O]): Process[I,O] = p match {
    case Halt() => Halt()
    case Emit(head, tail) => Emit(head, feed(oi)(tail))
    case Await(recv) => recv(oi)
  }

  def exists[I](p: I => Boolean): Process[I,Boolean] =
    lift(p) |> any

  def any: Process[Boolean, Boolean] =
    loop(false) {
      case (i, acc) => (i || acc, i || acc)
    }
}
