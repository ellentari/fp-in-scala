import fp.streamingio.Process._

val p = liftOne((x: Int) => x * 2)
val xs = p(Stream(1,2,3)).toList

val units = Stream.continually(())
val ones = lift((_: Unit) => 1)(units)

val even = filter((x: Int) => x % 2 == 0)
val evens = even(Stream(1,2,3,4)).toList

sum(Stream[Double](1, 2, 3, 4)).toList
sumViaLoop(Stream[Double](1, 2, 3, 4)).toList

mean(Stream[Double](1, 2, 3, 4)).toList

take(2)(Stream(1, 2, 3, 4)).toList
drop(2)(Stream(1, 2, 3, 4)).toList

takeWhile((i: Int) => i < 3)(Stream(1, 2, 3, 4)).toList
dropWhile((i: Int) => i < 3)(Stream(1, 2, 3, 4)).toList

count(Stream("a", "b", "c")).toList
countViaLoop(Stream("a", "b", "c")).toList

