package fp.streamingio

import java.io.PrintWriter

import fp.effect.IO
import fp.effect.IO.IO
import fp.streamingio.Process._
import fp.streamingio.ProcessF.{fileW, intersperse}

import scala.annotation.tailrec

object ProcessApp extends App {

  def toCelsius(fahrenheit: Double): Double =
    (5.0 / 9.0) * (fahrenheit - 32.0)

  def processFile0[A,B](file: java.io.File, p: Process[String,A], z: B)(f: (B, A) => B): IO[B] = IO {
    @tailrec
    def go(ss: Iterator[String], cur: Process[String, A], acc: B): B = cur match {
      case Halt() => acc
      case Emit(head, tail) => go(ss, tail, f(acc, head))
      case Await(recv) =>
        val next = if (ss.hasNext) recv(Some(ss.next())) else recv(None)
        go(ss, next, acc)
    }

    val s = io.Source.fromFile(file, "UTF-8")
    try go(s.getLines(), p, z)
    finally s.close()
  }

  def processFile1[A,B](src: java.io.File, p: Process[String,String], target: java.io.File): IO[Unit] = IO {
    @tailrec
    def go(ss: Iterator[String], cur: Process[String, String], writer: java.io.Writer): Unit = cur match {
      case Halt() => ()
      case Emit(head, tail) =>
        writer.write(head + "\n")
        go(ss, tail, writer)
      case Await(recv) =>
        val next = if (ss.hasNext) recv(Some(ss.next())) else recv(None)
        go(ss, next, writer)
    }

    def processWrite(ss: Iterator[String]): Unit = {
      val writer = new PrintWriter(target)
      try go(ss, p, writer)
      finally writer.close()
    }

    val s = io.Source.fromFile(src, "UTF-8")
    try processWrite(s.getLines())
    finally s.close()
  }

  def convertFahrenheit: Process[String, String] =
    filter[String](!_.trim.isEmpty) |>
      filter(!_.startsWith("#")) |>
      lift(_.toDouble) |>
      lift(toCelsius) |>
      lift(_.toString)

  val converter: ProcessF[IO, Unit] =
    ProcessF.lines("fahrenheit.txt").
      filter(!_.trim.isEmpty).
      filter(!_.startsWith("#")).
      map(line => toCelsius(line.toDouble).toString).
      |>(intersperse("\n")).
      to(fileW("celsius.txt")).
      drain

}
