package fp.stream

import Stream._

object Chapter5 extends App {

  println(Stream(1, 2, 3, 4, 5).toListRec)

  println(Stream(1, 2, 3, 4, 5).headOption)
  println(Stream(1, 2, 3, 4, 5).headOptionViaFoldRight)

  println(Stream(1, 2, 3, 4, 5).take(2).toList)
  println(Stream(1, 2, 3, 4, 5).drop(2).toList)

  println(Stream(1, 2, 3, 4, 5).takeWhile(_ <= 3).toList)

  println(Stream(1, 2, 3, 4, 5).takeWhileViaFoldRight(_ <= 3).toList)

  println("from:")
  println(from(5).take(5).toList)

  println("fromViaUnfold:")
  println(fromViaUnfold(5).take(5).toList)

  println("ones:")
  println(ones.take(5).toList)

  println("onesViaUnfold:")
  println(onesViaUnfold.take(5).toList)

  println("fibs:")
  println(fibs.take(7).toList)
  println("fibsViaUnfold:")
  println(fibsViaUnfold.take(7).toList)

  println("unfold:")
  println(unfold(1)(i => Some((i, i + 1))).take(5).toList)

  println("mapViaUnfold:")
  println(Stream(1, 2, 3, 4, 5).mapViaUnfold(_ + 1).toList)
  println("takeWhileViaUnfold:")
  println(Stream(1, 2, 3, 4, 5).takeWhileViaUnfold(_ < 4).toList)
  println("takeViaUnfold:")
  println(Stream(1, 2, 3, 4, 5).takeViaUnfold(3).toList)
  println("zipWith:")
  println(Stream(1, 2, 3, 4, 5).zipWith(Stream(1, 2, 3, 4, 5, 6))((_, _)).toList)
  println("zipAll:")
  println(Stream(1, 2, 3, 4, 5).zipAll(Stream(1, 2, 3, 4, 5, 6, 7)).toList)

  println("startsWith:")
  println(Stream(1, 2, 3).startsWith(Stream(1, 2)))
  println("startsWith:")
  println(Stream(1, 2, 3).startsWith(Stream(1, 2, 4)))
  println("startsWith:")
  println(Stream(1, 2, 3).startsWith(Stream(3, 5)))

  println("tails:")
  println(Stream(1, 2, 3, 4, 5).tails.map(_.toList).toList)

  println("hasSubsequence:")
  println(Stream(1, 2, 3, 4, 5).hasSubsequence(Stream(3, 4)))
  println("hasSubsequence:")
  println(Stream(1, 2, 3, 4, 5).hasSubsequence(Stream(3, 5)))

  println("scanRight:")
  println(Stream(1, 2, 3).scanRight(0)(_ + _).toList)


}
