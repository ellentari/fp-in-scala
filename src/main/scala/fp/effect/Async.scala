package fp.effect

import fp.parallelism.Par._


object Async {

  type Async[A] = Free[Par, A]

  @scala.annotation.tailrec
  def step[A](async: Async[A]): Async[A] = async match {
    case FlatMap(x, f: (Any => Async[A])) => x match {
      case Return(a) => step(f(a))
      case FlatMap(y, g: (Any => Async[Any])) =>
        step(y flatMap (a => g(a) flatMap (b => f(b))))
    }
    case _ => async
  }

  def run[A](async: Async[A]): Par[A] = step(async) match {
    case Return(a) => unit(a)
    case Suspend(r) => r
    case FlatMap(x, f: (Any => Async[A])) => x match {
      case Suspend(r) => flatMap(r)(a => run(f(a)))
      case _ => sys.error("Impossible; `step` eliminates these cases")
    }

  }
}
