package fp.effect

import fp.effect.Translate.~>
import fp.monad.Monad

import scala.annotation.tailrec

sealed trait Free[F[_], A] {

  def flatMap[B](f: A => Free[F, B]): Free[F, B] =
    FlatMap(this, f)

  def map[B](f: A => B): Free[F, B] =
    flatMap(f andThen (Return(_)))
}

case class Return[F[_],A](a: A) extends Free[F, A]
case class Suspend[F[_],A](s: F[A]) extends Free[F, A]
case class FlatMap[F[_],A,B](s: Free[F, A], f: A => Free[F, B]) extends Free[F, B]

object Free {

  implicit def freeMonad[F[_]]: Monad[({type f[x] = Free[F, x]})#f] =
    new Monad[({type f[x] = Free[F, x]})#f] {

      override def unit[A](a: => A): Free[F, A] = Return(a)
      override def flatMap[A, B](fa: Free[F, A])(f: A => Free[F, B]): Free[F, B] = fa.flatMap(f)
    }

  @tailrec
  def runTrampoline[A](free: Free[Function0, A]): A = free match {
    case Return(a) => a
    case Suspend(s) => s()
    case FlatMap(x, f: (Any => Free[Function0, A])) => x match {
      case Return(a) => runTrampoline(f(a))
      case Suspend(s) => runTrampoline(f(s()))
      case FlatMap(y, g: (Any => Free[Function0, Any])) =>
        runTrampoline(y flatMap (a => g(a) flatMap (f)))
    }
  }

  @scala.annotation.tailrec
  def step[F[_], A](free: Free[F, A]): Free[F, A] = free match {
    case FlatMap(x, f: (Any => Free[F, A])) => x match {
      case Return(a) => step(f(a))
      case FlatMap(y, g: (Any => Free[F, Any])) =>
        step(y flatMap (a => g(a) flatMap (f)))
    }
    case _ => free
  }

  def run[F[_], A](free: Free[F, A])(implicit F: Monad[F]): F[A] =
    runFree(free)(Translate.identity[F])

  def runFree[F[_], G[_], A](free: Free[F, A])(t: F ~> G)(implicit G: Monad[G]): G[A] = step(free) match {
    case Return(a) => G.unit(a)
    case Suspend(r) => t(r)
    case FlatMap(x, f: (Any => Free[F, A])) => x match {
      case Suspend(r) => G.flatMap(t(r))(a => runFree(f(a))(t))
      case _ => sys.error("Impossible; `step` eliminates these cases")
    }
  }

}
