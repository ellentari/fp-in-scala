package fp.effect

import fp.monad.Monad

import scala.annotation.tailrec
import scala.io.StdIn

object TailRec {

  type TailRec[A] = Free[Function0, A]

  implicit val tailRecMonad: Monad[TailRec] = new Monad[TailRec] {
    override def unit[A](a: => A): TailRec[A] = TailRec.unit(a)
    override def flatMap[A, B](fa: TailRec[A])(f: A => TailRec[B]): TailRec[B] = fa flatMap f
  }

  def unit[A](a: => A): TailRec[A] = Return(a)

  def empty: TailRec[Unit] = Return(())

  def apply[A](a: => A): TailRec[A] = unit(a)

  def printLine(s: String): TailRec[Unit] = Suspend(() => Return(print(s)))

  def readLine: TailRec[String] = TailRec { StdIn.readLine() }

  @tailrec
  def run[A](io: TailRec[A]): A = io match {
    case Return(a) => a
    case Suspend(r) => r()
    case FlatMap(x, f: (Any => TailRec[A])) => x match {
      case Return(a) => run(f(a))
      case Suspend(r) => run(f(r()))
      case FlatMap(y, g: (Any => TailRec[Any])) => run(y flatMap (a => g(a) flatMap (f)))
    }
  }

}
