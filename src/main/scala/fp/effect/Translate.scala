package fp.effect

trait Translate[F[_], G[_]] {
  def apply[A](f: F[A]): G[A]
}

object Translate {
  type ~> [F[_], G[_]] = Translate[F, G]

  def identity[F[_]]: F ~> F = new (F ~> F) {
    override def apply[A](f: F[A]): F[A] = f
  }

  def translate[F[_],G[_],A](f: Free[F, A])(fg: F ~> G): Free[G, A] = {
    type FreeG[B] = Free[G,B]
    val t = new (F ~> FreeG) {
      override def apply[B](f: F[B]): FreeG[B] = Suspend { fg(f) }
    }
    Free.runFree(f)(t)(Free.freeMonad)
  }
}
