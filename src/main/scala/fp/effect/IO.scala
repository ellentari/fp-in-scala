package fp.effect

import java.util.concurrent.ExecutorService

import fp.monad.Monad
import fp.parallelism.Nonblocking
import fp.parallelism.Nonblocking.Par

import scala.io.StdIn

object IO {

  type IO[A] = Free[Par, A]

  implicit val ioMonad: Monad[IO] = Free.freeMonad[Par]

  def unit[A](a: => A): IO[A] = Return(a)

  def empty: IO[Unit] = Return(())

  def apply[A](a: => A): IO[A] = unit(a)

  def printLine(s: String): IO[Unit] = Suspend(Nonblocking.unit(print(s)))

  def readLine: IO[String] = Return { StdIn.readLine() }

  def unsafePerformIO[A](io: IO[A])(implicit E: ExecutorService): A =
    Nonblocking.run(E) { Free.run(io)(Monad.nioParMonad) }

}
