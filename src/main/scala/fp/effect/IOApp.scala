package fp.effect

import fp.effect.TailRec._
import fp.monad.Monad

object IOApp extends App {

  case class Player(name: String, score: Int)

  def winner(p1: Player, p2: Player): Option[Player] =
    if (p1.score > p2.score) Some(p1)
    else if (p1.score < p2.score) Some(p2)
    else None

  def winnerMsg(p: Option[Player]): String = p map {
    case Player(name, _) => s"$name is the winner!"
  } getOrElse "It's a draw."

  def contest(p1: Player, p2: Player): TailRec[Unit] =
    printLine(winnerMsg(winner(p1, p2)))

  def fahrenheitToCelsius(f: Double): Double =
    (f - 32) * 5.0/9.0

  def converter(): TailRec[Unit] = {
    for {
      _ <- printLine("Enter a temperature in degrees Fahrenheit:")
      d <- readLine.map(_.toDouble)
      _ <- printLine(fahrenheitToCelsius(d).toString)
    } yield ()
  }

  implicitly[Monad[TailRec]].forever(printLine("Still going..."))

}
