package fp.effect

import fp.effect.Free.runFree
import fp.effect.Translate.~>
import fp.monad.Monad
import fp.monad.Monad._
import fp.parallelism.Par
import fp.parallelism.Par.Par

import scala.io.StdIn

sealed trait Console[A] {
  def toPar: Par[A]
  def toThunk: () => A
}

case object ReadLine extends Console[Option[String]] {
  override def toPar: Par[Option[String]] = Par.lazyUnit(run)
  override def toThunk: () => Option[String] = () => run

  def run: Option[String] =
    try Some(StdIn.readLine())
    catch { case e: Exception => None }
}

case class PrintLine(line: String) extends Console[Unit] {
  override def toPar: Par[Unit] = Par.lazyUnit(println(line))
  override def toThunk: () => Unit = () => println(line)
}

object Console {

  type ConsoleIO[A] = Free[Console, A]

  def readLn: ConsoleIO[Option[String]] =
    Suspend(ReadLine)

  def printLn(line: String): ConsoleIO[Unit] =
    Suspend(PrintLine(line))

  val consoleToFunction0: Console ~> Function0 = new (Console ~> Function0) {
    override def apply[A](f: Console[A]): () => A  = f.toThunk
  }

  val consoleToPar: Console ~> Par = new (Console ~> Par) {
    override def apply[A](f: Console[A]): Par[A] = f.toPar
  }

  implicit val function0Monad: Monad[Function0] = new Monad[Function0] {
    def unit[A](a: => A): () => A = () => a
    override def flatMap[A,B](a: () => A)(f: A => (() => B)): () => B =
      () => f(a())()
  }

  def runConsoleFunction0[A](a: Free[Console, A]): () => A =
    runFree(a)(consoleToFunction0)

  def runConsolePar[A](a: Free[Console, A]): Par[A] =
    runFree(a)(consoleToPar)

  def runConsole[A](a: Free[Console, A]): A =
    Free.runTrampoline {
      Translate.translate(a)(consoleToFunction0)
    }


}
