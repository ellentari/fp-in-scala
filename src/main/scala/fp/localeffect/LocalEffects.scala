package fp.localeffect

import fp.localeffect.QuickSort.qs

import scala.collection.mutable

trait ST[S, A] {self =>

  protected def run(s: S): (A, S)

  def map[B](f: A => B): ST[S, B] = new ST[S, B] {
    override protected def run(s: S): (B, S) = {
      val (a, s1) = self.run(s)
      (f(a), s1)
    }
  }

  def flatMap[B](f: A => ST[S, B]): ST[S, B] = new ST[S, B] {
    override protected def run(s: S): (B, S) = {
      val (a, s1) = self.run(s)
      f(a).run(s1)
    }
  }
}

object ST {

  def apply[S, A](a: => A): ST[S, A] = {
    lazy val memo = a
    new ST[S, A] {
      override protected def run(s: S): (A, S) = (memo, s)
    }
  }

  def runST[A](st: RunnableST[A]): A =
    st.apply[Unit].run(())._1
}

trait RunnableST[A] {
  def apply[S]: ST[S, A]
}

trait STRef[S, A] {

  protected var cell: A

  def read: ST[S, A] = ST(cell)

  def write(a: A): ST[S, Unit] = new ST[S, Unit] {
    override protected def run(s: S): (Unit, S) = {
      cell = a
      ((), s)
    }
  }
}

object STRef {

  def apply[S, A](a: A): ST[S, STRef[S, A]] = ST(new STRef[S, A] {
    override protected var cell: A = a
  })
}

sealed abstract class STArray[S, A](implicit manifest: Manifest[A]) {

  protected def value: Array[A]

  def size: ST[S, Int] = ST(value.length)

  def read(i: Int): ST[S, A] = ST(value(i))

  def freeze: ST[S, List[A]] = ST(value.toList)

  def write(i: Int, a: A): ST[S, Unit] = new ST[S, Unit] {
    override protected def run(s: S): (Unit, S) = {
      value(i) = a
      ((), s)
    }
  }

  def fill(xs: Map[Int, A]): ST[S, Unit] = new ST[S, Unit] {
    override protected def run(s: S): (Unit, S) = {
      xs foreach {
        case (index, a) => write(index, a)
      }
      ((), s)
    }
  }

  def swap(i: Int, j: Int): ST[S, Unit] = for {
    x <- read(i)
    y <- read(j)
    _ <- write(i, y)
    _ <- write(j, x)
  } yield ()
}

object STArray {

  def apply[S, A: Manifest](sz: Int, v: A): ST[S, STArray[S, A]] =
    ST(new STArray[S, A]() {
      lazy val value: Array[A] = Array.fill(sz)(v)
    })

  def fromList[S, A: Manifest](xs: List[A]): ST[S, STArray[S, A]] =
    ST(new STArray[S, A]() {
      lazy val value: Array[A] = xs.toArray
    })
}

sealed abstract class STHashMap[S, K, V] {

  protected def value: mutable.HashMap[K, V]

  def size: ST[S, Int] = ST(value.size)
  def apply(k: K): ST[S, V] = ST(value(k))
  def get(k: K): ST[S, Option[V]] = ST(value.get(k))
  def freeze: ST[S, Map[K, V]] = ST(value.toMap)
  def +=(kv: (K, V)): ST[S,Unit] = ST(value += kv)
  def -=(k: K): ST[S,Unit] = ST(value -= k)

}

object STMap {
  def empty[S, K, V]: ST[S, STHashMap[S, K, V]] = ST(new STHashMap[S, K, V] {
    val value: mutable.HashMap[K, V] = mutable.HashMap.empty[K, V]
  })

  def fromMap[S, K, V](m: Map[K, V]): ST[S, STHashMap[S, K, V]] = ST(new STHashMap[S, K, V] {
    val value: mutable.HashMap[K, V] = (mutable.HashMap.newBuilder[K, V] ++= m).result
  })
}


object QuickSort {

  def quicksort(xs: List[Int]): List[Int] =
    if (xs.isEmpty) xs
    else ST.runST(new RunnableST[List[Int]] {
      override def apply[S]: ST[S, List[Int]] = for {
        arr    <- STArray.fromList(xs)
        size   <- arr.size
        _      <- qs(arr, 0, size - 1)
        sorted <- arr.freeze
      } yield sorted
    })

  private def noop[S]: ST[S, Unit] = ST(())

  private def partition[S](arr: STArray[S, Int], left: Int, right: Int, pivot: Int): ST[S, Int] = for {
    pivotVal <- arr.read(pivot)
    _        <- arr.swap(pivot, right)
    jRef     <- STRef(left)

    _        <- (left until right).foldLeft(noop[S])((s, i) => for {
                _  <- s
                iVal <- arr.read(i)
                _    <-
                  if (iVal < pivotVal) for {
                    j <- jRef.read
                    _ <- arr.swap(i, j)
                    _ <- jRef.write(j + 1)
                  } yield () else noop[S]
              } yield ())

    x <- jRef.read
    _ <- arr.swap(x, right)

  } yield x

  private def qs[S](arr: STArray[S, Int], left: Int, right: Int): ST[S, Unit] =
    if (left < right) for {
      pivot <- partition(arr, left, right, left + (right - left) / 2)
      _  <- qs(arr, left, pivot - 1)
      _  <- qs(arr, pivot + 1, right)
    } yield ()
    else noop[S]
}