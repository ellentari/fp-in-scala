package fp.parsing

/**
  * JSON parsing example.
  */
object JSONExample extends App {
  val jsonTxt =
    """
{
  "Company name" : "Microsoft Corporation",
  "Ticker"  : "MSFT",
  "Active"  : true,
  "Price"   : 30.66,
  "Shares outstanding" : 8.38e9,
  "Related companies" : [ "HPQ", "IBM", "YHOO", "DELL", "GOOG" ]
}
"""

  val malformedJson1 =
    """
{
  "Company name" ; "Microsoft Corporation"
}
"""

  val malformedJson2 =
    """
[
  [ "HPQ", "IBM",
  "YHOO", "DELL" ++
  "GOOG"
  ]
]
"""

  val malformedJson3 =
    """
{
  "Company name" : adsfasdf
}
"""

  val P = fp.parsing.MyParsers

  def printResult[R](e: Either[ParseError, R]) =
    e.fold(e => println(MyParsers.errorMessage(e)), println)

  val json: Parser[JSON] = JSON.jsonParser(P)

  printResult {
    P.run(json)(jsonTxt)
  }

  println("--")

  printResult {
    P.run(json)(malformedJson1)
  }

  println("--")

  printResult {
    P.run(json)(malformedJson2)
  }

  println("--")

  printResult {
    P.run(json)(malformedJson3)
  }
}
