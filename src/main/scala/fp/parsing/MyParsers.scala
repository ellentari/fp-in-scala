package fp.parsing

import scala.util.matching.Regex

trait Parser[+A] {
  def apply(location: Location): Result[A]
}

trait Result[+A] {

  def mapError(f: ParseError => ParseError): Result[A] = this match {
    case Failure(error, isCommitted) => Failure(f(error), isCommitted)
    case _ => this
  }

  def uncommit: Result[A] = this match {
    case Failure(error, true) => Failure(error, isCommitted = false)
    case _ => this
  }

  def addCommit(isCommitted: Boolean): Result[A] = this match {
    case Failure(error, c) => Failure(error, c || isCommitted)
    case _ => this
  }

  def advanceSuccess(n: Int): Result[A] = this match {
    case Success(a, consumed) => Success(a, consumed + n)
    case _ => this
  }
}

case class Success[+A](get: A, charsConsumed: Int) extends Result[A]
case class Failure(get: ParseError, isCommitted: Boolean) extends Result[Nothing]

object MyParsers extends Parsers[Parser] {

  override def run[A](p: Parser[A])(input: String): Either[ParseError, A] =
    p(Location(input)) match {
      case Success(a, _) => Right(a)
      case Failure(e, _) => Left(e)
    }

  override def string(s: String): Parser[String] =
    location => {
      def firstNonmatchingIndex(s1: String, s2: String, offset: Int): Int = {
        var i = 0
        while (i < s1.length && i < s2.length) {
          if (s1.charAt(i + offset) != s2.charAt(i)) return i
          i += 1
        }
        if (s1.length - offset >= s2.length) -1
        else s1.length - offset
      }

      def expected(i: Int) =
        s.drop(i)

      def mismatched(i: Int) = {
        location.input.substring(location.offset + i, location.input.length min location.offset + s.length)
      }

      val i = firstNonmatchingIndex(location.input, s, location.offset)
      if (i == -1) Success(s, s.length)
      else Failure(location.advanceBy(i).toError(s"expected: '${expected(i)}', got: '${mismatched(i)}'"), i > 0)
    }

  override def regex(r: Regex): Parser[String] =
    location => {
      r.findPrefixOf(location.get) match {
        case Some(m) => Success(m, m.length)
        case None => Failure(location.toError(s"Expected to match regexp: <$r>"), isCommitted = false)
      }
    }

  override def slice[A](p: Parser[A]): Parser[String] =
    location =>
      p(location) match {
        case Success(_, consumed) => Success(location.get.slice(0, consumed), consumed)
        case e@Failure(_, _) => e
      }

  override def label[A](msg: String)(p: Parser[A]): Parser[A] =
    location => p(location).mapError(_.label(msg))

  override def scope[A](msg: String)(p: Parser[A]): Parser[A] =
    location => p(location).mapError(_.push(location, msg))

  override def flatMap[A, B](p: Parser[A])(f: A => Parser[B]): Parser[B] =
    location => p(location) match {
      case Success(a, consumed) => {
        f(a)(location.advanceBy(consumed))
          .addCommit(consumed != 0)
          .advanceSuccess(consumed)
      }
      case e@Failure(_, _) => e
    }

  override def attempt[A](p: Parser[A]): Parser[A] =
    location => p(location).uncommit

  override def or[A](p1: Parser[A], p2: => Parser[A]): Parser[A] =
    location => p1(location) match {
      case Failure(_, false) => p2(location)
      case r => r
    }

  override def succeed[A](a: A): Parser[A] = _ => Success(a, 0)

  override def errorLocation(e: ParseError): Location =
    e.latestLocation.get

  override def errorMessage(e: ParseError): String =
    e.stack map {case (location, msg) => s"$msg at [${location.line}, ${location.col}]: ${location.get}"} mkString "\n"
}
