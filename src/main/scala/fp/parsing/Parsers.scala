package fp.parsing

import java.util.regex.Pattern

import fp.testing.Gen

import scala.util.matching.Regex

trait Parsers[Parser[+_]] { self =>

  def run[A](p: Parser[A])(input: String): Either[ParseError, A]

  def string(s: String): Parser[String]

  def regex(r: Regex): Parser[String]

  def slice[A](p: Parser[A]): Parser[String]

  def label[A](msg: String)(p: Parser[A]): Parser[A]

  def scope[A](msg: String)(p: Parser[A]): Parser[A]

  def flatMap[A, B](p: Parser[A])(f: A => Parser[B]): Parser[B]

  def attempt[A](p: Parser[A]): Parser[A]

  def or[A](p1: Parser[A], p2: => Parser[A]): Parser[A]

  def succeed[A](a: A): Parser[A]

  def errorLocation(e: ParseError): Location

  def errorMessage(e: ParseError): String

  def map[A, B](p: Parser[A])(f: A => B): Parser[B] =
    p flatMap (a => succeed(f(a)))

  def map2[A, B, C](p1: Parser[A], p2: => Parser[B])(f: (A, B) => C): Parser[C] =
    for (
      a <- p1;
      b <- p2
    ) yield f(a, b)

  def product[A, B](p1: Parser[A], p2: => Parser[B]): Parser[(A, B)] =
    map2(p1, p2)((_, _))

  def listOfN[A](n: Int, p: Parser[A]): Parser[List[A]] =
    if (n <= 0) succeed(List())
    else map2(p, listOfN(n - 1, p))(_ :: _)

  def many[A](p: Parser[A]): Parser[List[A]] =
    many1(p) | succeed(List())

  def many1[A](p: Parser[A]): Parser[List[A]] =
    map2(p, many(p))(_ :: _)

  def separated[A](p: Parser[A], delimiter: Parser[Any]): Parser[List[A]] =
    separated1(p, delimiter) or succeed(List())

  def separated1[A](p: Parser[A], delimiter: Parser[Any]): Parser[List[A]] =
    map2(p, many(delimiter *> p))(_ :: _)

  def char(c: Char): Parser[Char] =
    string(c.toString) map (_.charAt(0))

  def skipRight[A](p: Parser[A], skipped: => Parser[Any]): Parser[A] =
    map2(p, skipped.slice)((a, _) => a)

  def skipLeft[A](skipped: Parser[Any], p: => Parser[A]): Parser[A] =
    map2(skipped.slice, p)((_, a) => a)

  def double: Parser[Double] =
    token(regex("[-+]?([0-9]*\\.)?[0-9]+([eE][-+]?[0-9]+)?".r)) map (_.toDouble) label "double literal"

  def thru(s: String): Parser[String] = (".*?" + Pattern.quote(s)).r

  def quotedString: Parser[String] =
    token(string("\"") *> thru("\"").map(_.dropRight(1)) label "string literal")

  def whitespace: Parser[String] = "\\s*".r

  def as[A](p: Parser[Any])(a: => A): Parser[A] =
    p.slice map (_ => a)

  def surround[A](start: Parser[Any], end: Parser[Any])(p: => Parser[A]): Parser[A] =
    start *> p <* end

  def token[A](p: Parser[A]): Parser[A] =
    attempt(p) <* whitespace

  def eof: Parser[String] = "\\z".r label "eof"

  def root[A](p: Parser[A]): Parser[A] =
    p <* eof

  implicit def string2Parser(s: String): Parser[String] = string(s)
  implicit def regex2Parser(r: Regex): Parser[String] = regex(r)
  implicit def ops[A](p: Parser[A]): ParserOps[A] = ParserOps[A](p)
  implicit def asStringParser[A](a: A)(implicit f: A => Parser[String]): ParserOps[String] = ops(f(a))

  case class ParserOps[A](p: Parser[A]) {
    def |[B >: A](p2: => Parser[B]): Parser[B] = self.or(p, p2)
    def or[B >: A](p2: => Parser[B]): Parser[B] = self.or(p, p2)
    def many: Parser[List[A]] = self.many(p)
    def many1: Parser[List[A]] = self.many1(p)
    def separated(delimiter: Parser[Any]): Parser[List[A]] = self.separated(p, delimiter)
    def separated1(delimiter: Parser[Any]): Parser[List[A]] = self.separated1(p, delimiter)
    def slice: Parser[String] = self.slice(p)
    def map[B](f: A => B): Parser[B] = self.map(p)(f)
    def flatMap[B](f: A => Parser[B]): Parser[B] = self.flatMap(p)(f)
    def **[B](p2: => Parser[B]): Parser[(A, B)] = self.product(p, p2)
    def product[B](p2: => Parser[B]): Parser[(A, B)] = self.product(p, p2)
    def *>[B](p2: => Parser[B]): Parser[B] = self.skipLeft(p, p2)
    def <*(p2: => Parser[Any]): Parser[A] = self.skipRight(p, p2)
    def as[B](b: => B): Parser[B] = self.map(self.slice(p))(_ => b)
    def label(msg: String): Parser[A] = self.label(msg)(p)
    def scope(msg: String): Parser[A] = self.scope(msg)(p)
    def attempt: Parser[A] = self.attempt(p)
  }

  object Laws {

    import fp.testing.Gen._

    def equal[A](p1: Parser[A], p2: Parser[A])(in: Gen[String]): Prop =
      forAll(in)(s => run(p1)(s) == run(p2)(s))

    def mapLaw[A](p: Parser[A])(in: Gen[String]): Prop =
      equal(p, p.map(a => a))(in)

    def succeedLaw[A](in: Gen[String]): Prop =
      forAll(in)(s => run(succeed("any"))(s) == Right("any"))
  }

}

case class Location(input: String, offset: Int = 0) {
  lazy val line: Int = input.slice(0, offset + 1).count(_ == '\n') + 1
  lazy val col: Int = input.slice(0, offset + 1).lastIndexOf('\n') match {
    case -1 => offset + 1
    case lineStart => offset - lineStart
  }

  def toError(msg: String): ParseError =
    ParseError(List((this, msg)))

  def get: String = input.slice(offset, input.length)

  def advanceBy(n: Int): Location =
    copy(offset = offset + n)
}

case class ParseError(stack: List[(Location, String)]) {

  def push(location: Location, msg: String): ParseError =
    copy(stack = (location, msg) :: stack)

  def label(msg: String): ParseError =
    ParseError(latestLocation.map((_, msg)).toList)

  def latestLocation: Option[Location] =
    latest map (_._1)

  def latest: Option[(Location, String)] =
    stack.lastOption
}