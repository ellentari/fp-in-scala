package fp.parsing

trait JSON

object JSON {
  case object JNull extends JSON
  case class JNumber(get: Double) extends JSON
  case class JString(get: String) extends JSON
  case class JBool(get: Boolean) extends JSON
  case class JArray(get: IndexedSeq[JSON]) extends JSON
  case class JObject(get: Map[String, JSON]) extends JSON

  def jsonParser[Parser[+ _]](P: Parsers[Parser]): Parser[JSON] = {
    import P.{string2Parser => _, _}

    implicit def string2Token(s: String): Parser[String] = token(P.string(s))

    def lit = ((quotedString map JString) |
      (double map JNumber) |
      "true".as(JBool(true)) |
      "false".as(JBool(false)) |
      "null".as(JNull)) scope "literal"

    def keyValue = quotedString ** (":" *> value)

    def array = surround("[", "]")(
      value separated "," map(vs => JArray(vs.toIndexedSeq))) scope "array"

    def obj = surround("{", "}")(
      keyValue separated "," map(kvs => JObject(kvs.toMap))) scope "object"

    def value: Parser[JSON] = (lit | obj | array) label "valid JSON value expected"

    root(whitespace *> (obj | array))
  }

}
