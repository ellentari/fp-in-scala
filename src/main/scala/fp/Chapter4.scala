package fp

object Chapter4 extends App {
  val option5 = Some(5)
  val option3 = Some(3)
  val none = None

  println(option5 filter (_ > 3))
  println(option5 filter (_ < 3))

  println(none getOrElse 1)
  println(option5 getOrElse 1)
  println(none orElse option5)
  println(option3 orElse option5)
}
