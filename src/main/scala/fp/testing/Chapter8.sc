import java.util.concurrent.{ExecutorService, Executors}

import fp.parallelism.Par
import fp.parallelism.Par.Par
import fp.testing.Gen
import fp.testing.Gen._
import fp.testing.Gen.Prop._

val smallInt = Gen.choose(-10, 10)

val maxProp = forAll(listOf1(smallInt)) { ns =>
  val max = ns.max
  !ns.exists(_ > max)
}

run(maxProp)

val sortedProp = forAll(listOf(smallInt)) { ns =>
  val sorted = ns.sorted

  ns.isEmpty || sorted.zip(sorted.tail).forall{
    case(previous, current) => previous <= current
  }
}

run(sortedProp)

val ES: ExecutorService = Executors.newCachedThreadPool

val parProp = forAll(Gen.unit(Par.unit(1)))(i =>
  Par.map(i)(_ + 1)(ES).get == Par.unit(2)(ES).get)

run(parProp)

val parProp2 = Prop.check {
  Par.equal(
    Par.map(Par.unit(1))(_ + 1),
    Par.unit(2)
  ) (ES).get
}

val S: Gen[ExecutorService] = weighted(
  choose(1, 4).map(Executors.newFixedThreadPool) -> .75,
  unit(Executors.newCachedThreadPool) -> .25
)

def forAllPar[A](g: Gen[A])(f: A => Par[Boolean]): Prop =
  forAll(S ** g) { case s ** a => f(a)(s).get }

def checkPar(p: Par[Boolean]): Prop =
  forAllPar(Gen.unit(()))(_ => p)

val parProp3 = checkPar (
  Par.equal(
    Par.map(Par.unit(1))(_ + 1),
    Par.unit(2)
  )
)

run(parProp3)

val pint = Gen.choose(0, 10) map Par.unit

val p4 = forAllPar(pint)(n => Par.equal(Par.map(n)(y => y), n))

run(p4)

val f = Gen.choose(0, 10) map Par.unit

val p5 = forAllPar(f)(n => Par.equal(Par.fork(n), n))

run(p5)
