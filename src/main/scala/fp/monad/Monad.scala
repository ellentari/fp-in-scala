package fp.monad

import fp.applicative.{Applicative, Traverse}
import fp.parallelism.Par.Par
import fp.parallelism.{Nonblocking, Par}
import fp.state.RNG.State
import fp.testing.Gen

trait Monad[F[_]] extends Applicative[F]{

  def flatMap[A, B](fa: F[A])(f: A => F[B]): F[B] =
    join(map(fa)(f))

  def join[A](ffa: F[F[A]]): F[A] =
    flatMap(ffa)(identity)

  override def map[A, B](fa: F[A])(f: A => B): F[B] =
    flatMap(fa)(a => unit(f(a)))

  override def map2[A,B,C](fa: F[A], fb: F[B])(f: (A, B) => C): F[C] =
    flatMap(fa)(a => map(fb)(b => f(a, b)))

  def compose[A,B,C](f: A => F[B], g: B => F[C]): A => F[C] =
    a => flatMap(f(a))(g)

  def flatMapViaCompose[A,B](fa: F[A])(f: A => F[B]): F[B] =
    compose((a: F[A]) => a, f)(fa)

  def composeViaMapJoin[A,B,C](f: A => F[B], g: B => F[C]): A => F[C] =
    a => join(map(f(a))(g))

  def forever[A,B](a: F[A]): F[B] = {
    lazy val t: F[B] = forever(a)
    flatMap(a)(_ => t)
  }
}

object Monad {

  case class Id[A](value: A) {
    def flatMap[B](f: A => Id[B]): Id[B] = f(value)
    def map[B](f: A => B): Id[B] = Id(f(value))
  }

  import fp.list.{List => MyList}
  import fp.stream.{Stream => MyStream}
  import fp.{Either => MyEither, Right => MyRight}

  implicit val optionMonad: Monad[Option] = new Monad[Option] {
    override def unit[A](a: => A): Option[A] = Option(a)
    override def flatMap[A, B](fa: Option[A])(f: A => Option[B]): Option[B] = fa.flatMap(f)
  }

  implicit val myListMonad: Monad[MyList] = new Monad[MyList] {
    override def unit[A](a: => A): MyList[A] = MyList(a)
    override def flatMap[A, B](fa: MyList[A])(f: A => MyList[B]): MyList[B] = MyList.flatMap(fa)(f)
  }

  implicit val listMonad: Monad[List] = new Monad[List] {
    override def unit[A](a: => A): List[A] = List(a)
    override def flatMap[A, B](fa: List[A])(f: A => List[B]): List[B] = fa.flatMap(f)
  }

  implicit val streamMonad: Monad[MyStream] = new Monad[MyStream] {
    override def unit[A](a: => A): MyStream[A] = MyStream(a)
    override def flatMap[A, B](fa: MyStream[A])(f: A => MyStream[B]): MyStream[B] = fa.flatMap(f)
  }

  implicit val genMonad: Monad[Gen] = new Monad[Gen] {
    override def unit[A](a: => A): Gen[A] = Gen.unit(a)
    override def flatMap[A, B](fa: Gen[A])(f: A => Gen[B]): Gen[B] = fa.flatMap(f)
  }

  implicit val parMonad: Monad[Par] = new Monad[Par] {
    override def unit[A](a: => A): Par[A] = Par.unit(a)
    override def flatMap[A, B](fa: Par[A])(f: A => Par[B]): Par[B] = Par.flatMap(fa)(f)
  }

  implicit val nioParMonad: Monad[Nonblocking.Par] = new Monad[Nonblocking.Par] {
    override def unit[A](a: => A): Nonblocking.Par[A] = Nonblocking.unit(a)
    override def flatMap[A, B](fa: Nonblocking.Par[A])(f: A => Nonblocking.Par[B]): Nonblocking.Par[B] =
      Nonblocking.flatMap(fa)(f)
  }

  def stateMonad[S]: Monad[({type f[x] = State[S, x]})#f] = new Monad[({type f[x] = State[S,x]})#f] {
    override def unit[A](a: => A): State[S, A] = State.unit(a)
    override def flatMap[A, B](fa: State[S, A])(f: A => State[S, B]): State[S, B] = fa.flatMap(f)
  }

  implicit val idMonad: Monad[Id] = new Monad[Id] {
    override def unit[A](a: => A): Id[A] = Id(a)
    override def flatMap[A, B](fa: Id[A])(f: A => Id[B]): Id[B] = fa.flatMap(f)
  }

  def eitherMonad[L]: Monad[({type f[x] = MyEither[L, x]})#f] = new Monad[({type f[x] = MyEither[L, x]})#f] {
    override def unit[A](a: => A): MyEither[L, A] = MyRight(a)
    override def flatMap[A, B](fa: MyEither[L, A])(f: A => MyEither[L, B]): MyEither[L, B] = fa.flatMap(f)
  }

  def composeM[F[_],G[_]](F: Monad[F], G: Monad[G], T: Traverse[G]): Monad[({type f[x] = F[G[x]]})#f] =
    new Monad[({type f[x] = F[G[x]]})#f] {
      override def unit[A](a: => A): F[G[A]] = F.unit(G.unit(a))
      override def flatMap[A, B](fa: F[G[A]])(f: A => F[G[B]]): F[G[B]] = {
        F.flatMap(fa)((ga: G[A]) => F.map(T.traverse(ga)(a => f(a))(F))(ggb => G.join(ggb)))
      }
    }


}
