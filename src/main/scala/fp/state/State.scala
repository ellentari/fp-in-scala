package fp.state

import scala.annotation.tailrec

trait RNG {
  def nextInt: (Int, RNG)
}

case class SimpleRNG(seed: Long) extends RNG {
  override def nextInt: (Int, RNG) = {
    val newSeed = (seed * 0x5DEECE66DL + 0xBL) & 0xFFFFFFFFFFFFL
    val nextRNG = SimpleRNG(newSeed)
    val n = (newSeed >>> 16).toInt
    (n, nextRNG)
  }
}

object RNG {

  def Simple(seed: Long): RNG = SimpleRNG(seed)

  def nonNegativeInt(rng: RNG): (Int, RNG) = {
    val (i, rng2) = rng.nextInt
    (if (i == Int.MinValue) 0 else i.abs, rng2)
  }

  def int: Rand[Int] = _.nextInt

  def double(rng: RNG): (Double, RNG) = {
    val (i, rng2) = nonNegativeInt(rng)
    (i / (Int.MinValue.toDouble + 1), rng2)
  }

  def doubleViaMap: Rand[Double] =
    map(nonNegativeInt)(_ / (Int.MinValue.toDouble + 1))

  def intDouble(rng: RNG): ((Int, Double), RNG) = {
    val (i1, rng2) = rng.nextInt
    val (i2, rng3) = double(rng2)
    ((i1, i2), rng3)
  }

  def doubleInt(rng: RNG): ((Double, Int), RNG) = {
    val (i1, rng2) = double(rng)
    val (i2, rng3) = rng2.nextInt
    ((i1, i2), rng3)
  }

  def double3(rng: RNG): ((Double, Double, Double), RNG) = {
    val (i1, rng2) = double(rng)
    val (i2, rng3) = double(rng2)
    val (i3, rng4) = double(rng3)
    ((i1, i2, i3), rng4)
  }

  def ints(count: Int)(rng: RNG): (List[Int], RNG) = {
    @tailrec
    def go(n: Int, rng: RNG, acc: List[Int]): (List[Int], RNG) = {
      if (n == 0) (acc.reverse, rng)
      else {
        val (i, rng2) = rng.nextInt
        go(n - 1, rng2, i :: acc)
      }
    }

    go(count, rng, Nil)
  }

  def intsViaSequence(count: Int): Rand[List[Int]] =
    sequence(List.fill(count)(int))


  type Rand[+A] = RNG => (A, RNG)

  def unit[A](a: A): Rand[A] =
    rng => (a, rng)

  def map[A, B](s: Rand[A])(f: A => B): Rand[B] =
    rng => {
      val (a, rng2) = s(rng)
      (f(a), rng2)
    }

  def mapViaFlatMap[A, B](s: Rand[A])(f: A => B): Rand[B] =
    flatMap(s)(a => unit(f(a)))

  def map2[A, B, C](ra: Rand[A], rb: Rand[B])(f: (A, B) => C): Rand[C] =
    rng => {
      val (a, rng2) = ra(rng)
      val (b, rng3) = rb(rng2)
      (f(a, b), rng3)
    }

  def map2ViaFlatMap[A, B, C](ra: Rand[A], rb: Rand[B])(f: (A, B) => C): Rand[C] =
    flatMap(ra)(a => mapViaFlatMap(rb)(b => f(a, b)))

  def flatMap[A, B](s: Rand[A])(f: A => Rand[B]): Rand[B] =
    rng => {
      val (a, rng2) = s(rng)
      f(a)(rng2)
    }

  def both[A, B](ra: Rand[A], rb: Rand[B]): Rand[(A, B)] =
    map2(ra, rb)((_, _))

  def intDoubleViaBoth: Rand[(Int, Double)] =
    both(int, double)

  def doubleIntViaBoth: Rand[(Double, Int)] =
    both(double, int)

  def sequence[A](rs: List[Rand[A]]): Rand[List[A]] =
    rs.foldRight(unit(List[A]()))((r, acc) => map2(r, acc)(_ :: _))

  def nonNegativeLessThan(n: Int): Rand[Int] =
    flatMap(nonNegativeInt) { i =>
      val mod = i % n
      if (i + (n - 1) - mod >= 0) unit(mod) else nonNegativeLessThan(n)
    }


  case class State[S, +A](run: S => (A, S)) {

    def map[B](f: A => B): State[S, B] =
      flatMap(a => State.unit(f(a)))

    def flatMap[B](f: A => State[S, B]): State[S, B] = State(s => {
        val (a, s2) = run(s)
        f(a).run(s2)
    })

    def map2[B, C](o: State[S, B])(f: (A, B) => C): State[S, C] =
      flatMap(a => o.map(b => f(a, b)))
  }

  object State {

    type Rand[A] = State[RNG, A]

    def unit[S, A](a: A): State[S, A] =
      State(s => (a, s))

    def sequence[S, A](ss: List[State[S, A]]): State[S, List[A]] =
      ss.foldRight(unit[S, List[A]](List()))((s, acc) => s.map2(acc)(_ :: _))

    def get[S]: State[S, S] = State(s => (s, s))
    def set[S](s: S): State[S, Unit] = State(_ => ((), s))

    def modify[S](f: S => S): State[S, Unit] = for {
      s <- get
      _ <- set(f(s))
    } yield ()
  }

  sealed trait Input
  case object Coin extends Input
  case object Turn extends Input

  case class Machine(locked: Boolean, candies: Int, coins: Int)

  object Machine {

    def acceptInput(input: Input)(machine: Machine): Machine = (input, machine) match {
      case (_, Machine(_, 0, _)) => machine
      case (Coin, Machine(true, candy, coin)) => Machine(locked=false, candy, coin + 1)
      case (Turn, Machine(false, candy, coin)) => Machine(locked=true, candy - 1, coin)
      case _ => machine
    }

    def simulateMachine(inputs: List[Input]): State[Machine, (Int, Int)] = {
      State.get[Machine]
        .map(machine => inputs.foldRight(machine)((i, m) => acceptInput(i)(m)))
        .map(machine => (machine.coins, machine.candies))
    }
  }



}
