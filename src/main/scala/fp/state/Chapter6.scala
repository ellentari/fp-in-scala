package fp.state

import RNG._
import fp.state.RNG.Machine.simulateMachine

object Chapter6 extends App {

  val rng = SimpleRNG(1)

  println("nonNegativeInt:")
  println(nonNegativeInt(rng))

  println("double:")
  println(double(rng))

  println("ints:")
  println(ints(5)(rng))

  println("intsViaSequence:")
  println(intsViaSequence(5)(rng))

  println("sequence:")
  println(sequence(List(doubleViaMap, doubleViaMap, doubleViaMap))(rng))

  println(simulateMachine(List(Coin, Turn, Coin, Turn, Coin, Turn, Coin, Turn)).run(Machine(locked=false, 5, 10)))

}
