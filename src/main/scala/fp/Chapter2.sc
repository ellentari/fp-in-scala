import scala.annotation.tailrec

def fib(n: Int): Int = {
  @tailrec
  def loop(prev: Int, cur: Int, n: Int): Int = {
    if (n == 0) cur
    else loop(cur, cur + prev, n - 1)
  }

  loop(0, 1, n)
}

fib(5)


def isSorted[A](as: Array[A], ordered: (A, A) => Boolean): Boolean = {
  @tailrec
  def loop(i: Int, acc: Boolean): Boolean = {
    if (!acc) false
    else if (as.length <= i + 1) true
    else loop(i + 1, acc && ordered(as(i), as(i + 1)))
  }

  loop(0, acc=true)
}

isSorted(Array(1, 2, 3, 4, 5), (i1: Int, i2: Int) => i1 < i2)


def curry[A, B, C](f: (A, B) => C): A => (B => C) = {
  (a: A) => (b: B) => f(a, b)
}


val curried = curry((a1: Int, a2: Int) => a1 + a2)
curried(2)(3)


def uncurry[A, B, C](f: A => B => C): (A, B) => C = {
  (a: A, b: B) => f(a)(b)
}

val uncurried = uncurry(curried)
uncurried(2, 3)


def compose[A, B, C](f: B => C, g: A => B): A => C = {
  (a: A) => f(g(a))
}

val triple: (Int => Int) = _ * 3
val plus1: (Int => Int) = _ + 1

compose(plus1, triple)(3)


