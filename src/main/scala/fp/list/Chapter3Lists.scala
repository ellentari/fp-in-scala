package fp.list

import fp.list.List._
import fp.stream

object Chapter3Lists extends App {

  val list = fp.list.List(1, 2, 3, 4, 5)

  println(list)

  println()

  println("sumViaFoldRight: ")
  println(sumViaFoldRight(list))

  println()

  println("sumViaFoldLeft: ")
  println(sumViaFoldRight(list))

  println()

  println("productViaFoldRight: ")
  println(productViaFoldRight(list))

  println()

  println("productViaFoldLeft: ")
  println(productViaFoldLeft(list))

  println()

  println("drop 2: ")
  println(drop(list, 2))

  println()

  println("dropWhile x < 4:")
  println(dropWhile(list, (x: Int) => x < 4))

  println()

  println("init: ")
  println(init(list))

  println()

  println("lengthViaFoldRight:")
  println(lengthViaFoldRight(list))

  println()

  println("lengthViaFoldLeft:")
  println(lengthViaFoldLeft(list))

  println()

  println("reverse:")
  println(reverse(list))

  println()

  println("foldRight:")
  println(foldRight(list, fp.list.Nil: fp.list.List[Int])((el, acc) => Cons(el, acc)))

  println()

  println("foldRightViaFoldLeft:")
  println(foldRightViaFoldLeft(list, fp.list.Nil: fp.list.List[Int])((el, acc) => Cons(el, acc)))

  println()

  println("foldLeft:")
  println(foldLeft(list, fp.list.Nil: fp.list.List[Int])((acc, el) => Cons(el, acc)))

  println()

  println("foldLeftViaFoldRight:")
  println(foldLeftViaFoldRight(list, fp.list.Nil: fp.list.List[Int])((acc, el) => Cons(el, acc)))

  println()

  println("append:")
  println(append(list, fp.list.List(6, 7, 8, 9, 10)))

  println()

  println("appendViaFoldLeft:")
  println(appendViaFoldRight(list, fp.list.List(6, 7, 8, 9, 10)))

  println()

  println("concatenate:")
  println(concatenate(fp.list.List(fp.list.List(1, 2, 3), fp.list.List(4, 5), fp.list.List(6, 7))))

  println()

  println("concatenateViaFoldLeft:")
  println(concatenateViaFoldLeft(fp.list.List(fp.list.List(1, 2, 3), fp.list.List(4, 5), fp.list.List(6, 7))))

  println()

  println("plus1:")
  println(plus1(list))

  println()

  println("doublesToString:")
  println(doublesToString(fp.list.List(1.1, 2.2, 3.3, 4.4, 5.5)))

  println()

  println("map plus one:")
  println(map(list)(_ + 1))

  println()

  println("filter even:")
  println(filter(list)(_ % 2 == 0))

  println()

  println("flatMap:")
  println(flatMap(fp.list.List(1, 2, 3))(i => fp.list.List(i, i)))

  println()

  println("filterViaFlatMap:")
  println(filterViaFlatMap(list)(_ % 2 == 0))

  println()

  println("addCorresponding:")
  println(addCorresponding(fp.list.List(1, 2, 3), fp.list.List(1, 2, 3, 4)))

  println()

  println("addCorrespondingViaZipWith:")
  println(addCorrespondingViaZipWith(fp.list.List(1, 2, 3), fp.list.List(1, 2, 3, 4)))

  println()

  println("hasSubSequence:")
  println(hasSubSequence(fp.list.List(1, 1, 1, 3, 4), fp.list.List(1, 1, 3, 4)))

  println()

  println("hasSubSequence:")
  println(hasSubSequence(fp.list.List(1, 2, 3, 4), fp.list.List(2, 3)))




}
