package fp.list

import fp.stream

import scala.annotation.tailrec

sealed trait List[+A] {
  override def toString: String = this match {
    case Nil => ""
    case Cons(x, xs) => x + " " + xs.toString()
  }
}

case object Nil extends List[Nothing]
case class Cons[+A](head: A, tail: List[A]) extends List[A]


object List {

  def sum(ints: List[Int]): Int = ints match {
    case Nil => 0
    case Cons(x, xs) => x + sum(xs)
  }

  def sumViaFoldRight(ints: List[Int]): Int = foldRight(ints, 0)(_ + _)

  def sumViaFoldLeft(ints: List[Int]): Int = foldLeft(ints, 0)(_ + _)

  def product(ints: List[Double]): Double = ints match {
    case Nil => 1.0
    case Cons(0.0, _) => 0.0
    case Cons(x, xs) => x * product(xs)
  }

  def productViaFoldRight(ints: List[Int]): Int = foldRight(ints, 1)(_ * _)

  def productViaFoldLeft(ints: List[Int]): Int = foldLeft(ints, 1)(_ * _)

  def apply[A](as: A*): List[A] =
    if (as.isEmpty) Nil
    else Cons(as.head, apply(as.tail: _*))

  def tail[A](as: List[A]): List[A] = as match {
    case Nil => Nil
    case Cons(_, xs) => xs
  }

  def init[A](as: List[A]): List[A] = as match {
    case Nil => Nil
    case Cons(x, Cons(_, Nil)) => Cons(x, Nil)
    case Cons(x, xs) => Cons(x, init(xs))
  }

  def setHead[A](list: List[A], head: A): List[A] = Cons(head, list)

  @tailrec
  def drop[A](as: List[A], n: Int): List[A] = {
    if (n == 0) as
    else as match {
      case Nil => Nil
      case Cons(_, xs) => drop(xs, n - 1)
    }
  }

  @tailrec
  def dropWhile[A](as: List[A], f: A => Boolean): List[A] = as match {
      case Cons(x, xs) => if (f(x)) dropWhile(xs, f) else as
      case _ => as
  }

  def foldRight[A,B](as: List[A], z: B)(f: (A, B) => B): B = as match {
      case Nil => z
      case Cons(x, xs) => f(x, foldRight(xs, z)(f))
  }

  @tailrec
  def foldLeft[A,B](as: List[A], z: B)(f: (B, A) => B): B = as match {
      case Nil => z
      case Cons(x, xs) => foldLeft(xs, f(z, x))(f)
  }

  def foldLeftViaFoldRight[A,B](as: List[A], z: B)(f: (B, A) => B): B =
    foldRight(as, (b:B) => b)((h, acc) => b => acc(f(b, h)))(z)

  def foldRightViaFoldLeft[A,B](as: List[A], z: B)(f: (A, B) => B): B =
    foldLeft(reverse(as), z)((acc, h) => f(h, acc))

  def lengthViaFoldRight[A](as: List[A]): Int =
    foldRight(as, 0)((_, acc) => acc + 1)

  def lengthViaFoldLeft[A](as: List[A]): Int =
    foldLeft(as, 0)((acc, _) => acc + 1)

  def reverse[A](as: List[A]): List[A] =
    foldLeft(as, Nil: List[A])((acc, h) => Cons(h, acc))

  def append[A](a1: List[A], a2: List[A]): List[A] = a1 match {
      case Nil => a2
      case Cons(a, as) => Cons(a, append(as, a2))
  }

  def appendViaFoldRight[A](a1: List[A], a2: List[A]): List[A] =
    foldRight(a1, a2)(Cons(_,_))

  def concatenate[A](as: List[List[A]]): List[A] = as match {
      case Nil => Nil
      case Cons(head, tail) => append(head, concatenate(tail))
  }

  def concatenateViaFoldLeft[A](as: List[List[A]]): List[A] =
    foldLeft(as, Nil: List[A])(append)

  def plus1(as: List[Int]): List[Int] =
    foldRightViaFoldLeft(as, Nil: List[Int])((a, b) => Cons(a + 1, b))

  def doublesToString(as: List[Double]): List[String] =
    foldRightViaFoldLeft(as, Nil: List[String])((a, b) => Cons(a.toString, b))

  def map[A, B](as: List[A])(f: A => B): List[B] =
    foldRightViaFoldLeft(as, Nil: List[B])((a, b) => Cons(f(a), b))

  def filter[A](as: List[A])(p: A => Boolean): List[A] =
    foldRightViaFoldLeft(as, Nil: List[A])((a, b) => if (p(a)) Cons(a, b) else b)

  def flatMap[A, B](as: List[A])(f: A => List[B]): List[B] =
    foldRightViaFoldLeft(as, Nil: List[B])((a, b) => append(f(a), b))

  def filterViaFlatMap[A](as: List[A])(p: A => Boolean): List[A] =
    flatMap(as)(a => if (p(a)) List(a) else Nil: List[A])

  def addCorresponding(a1: List[Int], a2: List[Int]): List[Int] = (a1, a2) match {
    case (Nil, _) => Nil
    case (_, Nil) => Nil
    case (Cons(h1, t1), Cons(h2, t2)) => Cons(h1 + h2, addCorresponding(t1, t2))
  }

  def zipWith[A, B, C](a1: List[A], a2: List[B])(f: (A, B) => C): List[C] = (a1, a2) match {
    case (Nil, _) => Nil
    case (_, Nil) => Nil
    case (Cons(h1, t1), Cons(h2, t2)) => Cons(f(h1, h2), zipWith(t1, t2)(f))
  }

  def addCorrespondingViaZipWith(a1: List[Int], a2: List[Int]): List[Int] =
    zipWith(a1, a2)(_ + _)

  def hasSubSequence[A](list: List[A], subSeq: List[A]): Boolean = (list, subSeq) match {
    case (_, Nil) => true
    case (Nil, _) => false
    case (Cons(h1, t1), Cons(h2, t2)) => ((h1 == h2) && hasSubSequence(t1, t2)) || hasSubSequence(t1, subSeq)
  }
}
